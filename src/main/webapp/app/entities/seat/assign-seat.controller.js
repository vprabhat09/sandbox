(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('AssignSeatController', AssignSeatController);

    AssignSeatController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity','AssignSeat', 'Seat', 'Employee', 'Tcsmodule'];

    function AssignSeatController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity,AssignSeat, Seat, Employee, Tcsmodule) {
        var vm = this;

        vm.seat = entity;
        vm.clear = clear;
        vm.assignSeat = assignSeat;
        vm.employees = Employee.query({filter: 'seat-is-null'});
        var seatsDetails =   AssignSeat.get(function (){

            vm.seats = seatsDetails;
        });
        $q.all([vm.seat.$promise, vm.employees.$promise]).then(function() {
            if (!vm.seat.employee || !vm.seat.employee.id) {
                return $q.reject();
            }
            return Employee.get({id : vm.seat.employee.id}).$promise;
        }).then(function(employee) {
            vm.employees.push(employee);
        });
        vm.tcsmodules = Tcsmodule.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function assignSeat (){
            vm.isSaving = true;
           // vm.seat = vm.se
            var updateSeat = {};
            updateSeat["id"] =vm.seat.id["id"];
            updateSeat["employee"]=vm.seat.employee;
            updateSeat["module"]= vm.seat.module;

            if (vm.seat.id !== null) {
                Seat.update(updateSeat, onSaveSuccess, onSaveError);
            } else {
                Seat.save(vm.seat, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('modulemanagementV3App:seatUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
