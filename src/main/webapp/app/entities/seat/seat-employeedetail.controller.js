(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('SeatEmpDetailController', SeatEmpDetailController);

    SeatEmpDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Seat', 'Employee', 'Tcsmodule'];

    function SeatEmpDetailController($scope, $rootScope, $stateParams, previousState, entity, Seat, Employee, Tcsmodule) {
        var vm = this;

        vm.employee = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('modulemanagementV3App:seatUpdate', function(event, result) {
            vm.seat = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
