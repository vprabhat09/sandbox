(function() {
    'use strict';
    angular
        .module('modulemanagementV3App')
        .factory('Seat', Seat);
    angular
        .module('modulemanagementV3App')
        .factory('AssignSeat', AssignSeat);

    Seat.$inject = ['$resource'];
    AssignSeat.$inject = ['$resource'];

    function AssignSeat ($resource) {
        var resourceUrl =  'api/seats/getEmptySeats';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                isArray:true,
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
    function Seat ($resource) {
        var resourceUrl =  'api/seats/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
