(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('SeatDetailController', SeatDetailController);

    SeatDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Seat', 'Employee', 'Tcsmodule'];

    function SeatDetailController($scope, $rootScope, $stateParams, previousState, entity, Seat, Employee, Tcsmodule) {
        var vm = this;

        vm.seat = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('modulemanagementV3App:seatUpdate', function(event, result) {
            vm.seat = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
