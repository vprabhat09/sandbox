(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('SeatMapController', SeatMapController);

    SeatMapController.$inject = ['$scope', '$state', 'Seat'];

    function SeatMapController ($scope, $state, Seat) {
        var vm = this;

        vm.seats = [];

        loadAll();

        function loadAll() {
            Seat.query(function(result) {
                vm.seats = result;
            });
        }
    }
})();
