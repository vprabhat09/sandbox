(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('SeatDialogController', SeatDialogController);

    SeatDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Seat', 'Employee', 'Tcsmodule'];

    function SeatDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Seat, Employee, Tcsmodule) {
        var vm = this;

        vm.seat = entity;
        vm.clear = clear;
        vm.save = save;
        vm.employees = Employee.query({filter: 'seat-is-null'});
        $q.all([vm.seat.$promise, vm.employees.$promise]).then(function() {
            if (!vm.seat.employee || !vm.seat.employee.id) {
                return $q.reject();
            }
            return Employee.get({id : vm.seat.employee.id}).$promise;
        }).then(function(employee) {
            vm.employees.push(employee);
        });
        vm.tcsmodules = Tcsmodule.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;

            if (vm.seat.id !== null) {
                Seat.update(vm.seat, onSaveSuccess, onSaveError);
            } else {
                Seat.save(vm.seat, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('modulemanagementV3App:seatUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
