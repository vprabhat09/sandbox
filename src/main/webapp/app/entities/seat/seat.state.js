(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('seat', {
            parent: 'entity',
            url: '/seat',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Seats'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/seat/seats.html',
                    controller: 'SeatController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('seat-detail', {
            parent: 'entity',
            url: '/seat/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Seat'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/seat/seat-detail.html',
                    controller: 'SeatDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Seat', function($stateParams, Seat) {
                    return Seat.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'seat',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('seat-detail.edit', {
            parent: 'seat-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/seat/seat-dialog.html',
                    controller: 'SeatDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Seat', function(Seat) {
                            return Seat.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('seat.new', {
            parent: 'seat',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/seat/seat-dialog.html',
                    controller: 'SeatDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                seatId: null,
                                seatName: null,
                                status: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('seat', null, { reload: true });
                }, function() {
                    $state.go('seat');
                });
            }]
        })


            .state('seat.seatMap', {
                parent: 'entity',
                url: '/seat/seatMap',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Seat'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/seat/SeatMap.html',
                        controller: 'SeatMapController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {

                }
            })
            .state('seat.seatEmpDetails', {
                parent: 'entity',
                url: '/{empId}/employee',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Seat'
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/seat/seat-empDetail.html',
                        controller: 'SeatEmpDetailController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['$stateParams', 'Employee', function($stateParams, Employee) {
                                return Employee.get({id : $stateParams.id}).$promise;
                            }],

                        }
                    });
                }],
                views: {
                    'content@': {
                        templateUrl: 'app/entities/seat/SeatMap.html',
                        controller: 'SeatMapController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Employee', function($stateParams, Employee) {
                        return Employee.get({id : $stateParams.id}).$promise;
                    }],

                }
            })
            .state('seat.assignSeat', {
                parent: 'seat',
                url: '/assignSeat',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/seat/AssignSeat.html',
                        controller: 'AssignSeatController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    seatId: null,
                                    seatName: null,
                                    status: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function() {
                        $state.go('seat', null, { reload: true });
                    }, function() {
                        $state.go('seat');
                    });
                }]
            })
        .state('seat.edit', {
            parent: 'seat',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/seat/seat-dialog.html',
                    controller: 'SeatDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Seat', function(Seat) {
                            return Seat.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('seat', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('seat.delete', {
            parent: 'seat',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/seat/seat-delete-dialog.html',
                    controller: 'SeatDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Seat', function(Seat) {
                            return Seat.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('seat', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
