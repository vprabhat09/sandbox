(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('SeatController', SeatController);

    SeatController.$inject = ['$scope', '$state', 'Seat'];

    function SeatController ($scope, $state, Seat) {
        var vm = this;
        
        vm.seats = [];

        loadAll();

        function loadAll() {
            Seat.query(function(result) {
                vm.seats = result;
            });
        }
    }
})();
