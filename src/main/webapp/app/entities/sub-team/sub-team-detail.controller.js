(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('SubTeamDetailController', SubTeamDetailController);

    SubTeamDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SubTeam', 'Team'];

    function SubTeamDetailController($scope, $rootScope, $stateParams, previousState, entity, SubTeam, Team) {
        var vm = this;

        vm.subTeam = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('modulemanagementV3App:subTeamUpdate', function(event, result) {
            vm.subTeam = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
