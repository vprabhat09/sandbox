(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('sub-team', {
            parent: 'entity',
            url: '/sub-team',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'SubTeams'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/sub-team/sub-teams.html',
                    controller: 'SubTeamController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('sub-team-detail', {
            parent: 'entity',
            url: '/sub-team/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'SubTeam'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/sub-team/sub-team-detail.html',
                    controller: 'SubTeamDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'SubTeam', function($stateParams, SubTeam) {
                    return SubTeam.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'sub-team',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('sub-team-detail.edit', {
            parent: 'sub-team-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-team/sub-team-dialog.html',
                    controller: 'SubTeamDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SubTeam', function(SubTeam) {
                            return SubTeam.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('sub-team.new', {
            parent: 'sub-team',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-team/sub-team-dialog.html',
                    controller: 'SubTeamDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                subTeamId: null,
                                subTeamName: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('sub-team', null, { reload: true });
                }, function() {
                    $state.go('sub-team');
                });
            }]
        })
        .state('sub-team.edit', {
            parent: 'sub-team',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-team/sub-team-dialog.html',
                    controller: 'SubTeamDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SubTeam', function(SubTeam) {
                            return SubTeam.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('sub-team', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('sub-team.delete', {
            parent: 'sub-team',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/sub-team/sub-team-delete-dialog.html',
                    controller: 'SubTeamDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SubTeam', function(SubTeam) {
                            return SubTeam.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('sub-team', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
