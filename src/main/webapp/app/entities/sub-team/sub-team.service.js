(function() {
    'use strict';
    angular
        .module('modulemanagementV3App')
        .factory('SubTeam', SubTeam);

    SubTeam.$inject = ['$resource'];

    function SubTeam ($resource) {
        var resourceUrl =  'api/sub-teams/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
