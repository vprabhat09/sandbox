(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('SubTeamController', SubTeamController);

    SubTeamController.$inject = ['$scope', '$state', 'SubTeam'];

    function SubTeamController ($scope, $state, SubTeam) {
        var vm = this;
        
        vm.subTeams = [];

        loadAll();

        function loadAll() {
            SubTeam.query(function(result) {
                vm.subTeams = result;
            });
        }
    }
})();
