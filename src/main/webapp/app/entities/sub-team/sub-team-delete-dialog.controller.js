(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('SubTeamDeleteController',SubTeamDeleteController);

    SubTeamDeleteController.$inject = ['$uibModalInstance', 'entity', 'SubTeam'];

    function SubTeamDeleteController($uibModalInstance, entity, SubTeam) {
        var vm = this;

        vm.subTeam = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SubTeam.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
