(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('SubTeamDialogController', SubTeamDialogController);

    SubTeamDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SubTeam', 'Team'];

    function SubTeamDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SubTeam, Team) {
        var vm = this;

        vm.subTeam = entity;
        vm.clear = clear;
        vm.save = save;
        vm.teams = Team.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.subTeam.id !== null) {
                SubTeam.update(vm.subTeam, onSaveSuccess, onSaveError);
            } else {
                SubTeam.save(vm.subTeam, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('modulemanagementV3App:subTeamUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
