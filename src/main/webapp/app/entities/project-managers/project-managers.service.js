(function() {
    'use strict';
    angular
        .module('modulemanagementV3App')
        .factory('ProjectManagers', ProjectManagers);

    ProjectManagers.$inject = ['$resource'];

    function ProjectManagers ($resource) {
        var resourceUrl =  'api/project-managers/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
