(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('ProjectManagersDialogController', ProjectManagersDialogController);

    ProjectManagersDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'ProjectManagers', 'Employee', 'Project'];

    function ProjectManagersDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, ProjectManagers, Employee, Project) {
        var vm = this;

        vm.projectManagers = entity;
        vm.clear = clear;
        vm.save = save;
        vm.employees = Employee.query({filter: 'projectmanager-is-null'});
        $q.all([vm.projectManagers.$promise, vm.employees.$promise]).then(function() {
            if (!vm.projectManagers.employeeId) {
                return $q.reject();
            }
            return Employee.get({id : vm.projectManagers.employeeId}).$promise;
        }).then(function(employee) {
            vm.employees.push(employee);
        });
        vm.teams = Project.query({filter: 'projectmanagers-is-null'});
        $q.all([vm.projectManagers.$promise, vm.teams.$promise]).then(function() {
            if (!vm.projectManagers.teamId) {
                return $q.reject();
            }
            return Project.get({id : vm.projectManagers.teamId}).$promise;
        }).then(function(team) {
            vm.teams.push(team);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.projectManagers.id !== null) {
                ProjectManagers.update(vm.projectManagers, onSaveSuccess, onSaveError);
            } else {
                ProjectManagers.save(vm.projectManagers, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('modulemanagementV3App:projectManagersUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
