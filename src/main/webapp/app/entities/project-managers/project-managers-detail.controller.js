(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('ProjectManagersDetailController', ProjectManagersDetailController);

    ProjectManagersDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ProjectManagers', 'Employee', 'Project'];

    function ProjectManagersDetailController($scope, $rootScope, $stateParams, previousState, entity, ProjectManagers, Employee, Project) {
        var vm = this;

        vm.projectManagers = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('modulemanagementV3App:projectManagersUpdate', function(event, result) {
            vm.projectManagers = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
