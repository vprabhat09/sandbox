(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('ProjectManagersDeleteController',ProjectManagersDeleteController);

    ProjectManagersDeleteController.$inject = ['$uibModalInstance', 'entity', 'ProjectManagers'];

    function ProjectManagersDeleteController($uibModalInstance, entity, ProjectManagers) {
        var vm = this;

        vm.projectManagers = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ProjectManagers.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
