(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('ProjectManagersController', ProjectManagersController);

    ProjectManagersController.$inject = ['$scope', '$state', 'ProjectManagers'];

    function ProjectManagersController ($scope, $state, ProjectManagers) {
        var vm = this;
        
        vm.projectManagers = [];

        loadAll();

        function loadAll() {
            ProjectManagers.query(function(result) {
                vm.projectManagers = result;
            });
        }
    }
})();
