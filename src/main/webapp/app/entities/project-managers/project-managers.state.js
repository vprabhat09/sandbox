(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('project-managers', {
            parent: 'entity',
            url: '/project-managers',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ProjectManagers'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/project-managers/project-managers.html',
                    controller: 'ProjectManagersController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('project-managers-detail', {
            parent: 'entity',
            url: '/project-managers/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'ProjectManagers'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/project-managers/project-managers-detail.html',
                    controller: 'ProjectManagersDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'ProjectManagers', function($stateParams, ProjectManagers) {
                    return ProjectManagers.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'project-managers',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('project-managers-detail.edit', {
            parent: 'project-managers-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/project-managers/project-managers-dialog.html',
                    controller: 'ProjectManagersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ProjectManagers', function(ProjectManagers) {
                            return ProjectManagers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('project-managers.new', {
            parent: 'project-managers',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/project-managers/project-managers-dialog.html',
                    controller: 'ProjectManagersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                projectManagerId: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('project-managers', null, { reload: true });
                }, function() {
                    $state.go('project-managers');
                });
            }]
        })
        .state('project-managers.edit', {
            parent: 'project-managers',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/project-managers/project-managers-dialog.html',
                    controller: 'ProjectManagersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ProjectManagers', function(ProjectManagers) {
                            return ProjectManagers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('project-managers', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('project-managers.delete', {
            parent: 'project-managers',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/project-managers/project-managers-delete-dialog.html',
                    controller: 'ProjectManagersDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ProjectManagers', function(ProjectManagers) {
                            return ProjectManagers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('project-managers', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
