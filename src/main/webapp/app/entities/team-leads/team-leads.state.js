(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('team-leads', {
            parent: 'entity',
            url: '/team-leads',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'TeamLeads'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/team-leads/team-leads.html',
                    controller: 'TeamLeadsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('team-leads-detail', {
            parent: 'entity',
            url: '/team-leads/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'TeamLeads'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/team-leads/team-leads-detail.html',
                    controller: 'TeamLeadsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'TeamLeads', function($stateParams, TeamLeads) {
                    return TeamLeads.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'team-leads',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('team-leads-detail.edit', {
            parent: 'team-leads-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/team-leads/team-leads-dialog.html',
                    controller: 'TeamLeadsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TeamLeads', function(TeamLeads) {
                            return TeamLeads.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('team-leads.new', {
            parent: 'team-leads',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/team-leads/team-leads-dialog.html',
                    controller: 'TeamLeadsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                teamLeadId: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('team-leads', null, { reload: true });
                }, function() {
                    $state.go('team-leads');
                });
            }]
        })
        .state('team-leads.edit', {
            parent: 'team-leads',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/team-leads/team-leads-dialog.html',
                    controller: 'TeamLeadsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TeamLeads', function(TeamLeads) {
                            return TeamLeads.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('team-leads', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('team-leads.delete', {
            parent: 'team-leads',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/team-leads/team-leads-delete-dialog.html',
                    controller: 'TeamLeadsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TeamLeads', function(TeamLeads) {
                            return TeamLeads.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('team-leads', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
