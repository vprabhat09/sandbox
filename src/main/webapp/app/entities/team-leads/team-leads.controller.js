(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TeamLeadsController', TeamLeadsController);

    TeamLeadsController.$inject = ['$scope', '$state', 'TeamLeads'];

    function TeamLeadsController ($scope, $state, TeamLeads) {
        var vm = this;
        
        vm.teamLeads = [];

        loadAll();

        function loadAll() {
            TeamLeads.query(function(result) {
                vm.teamLeads = result;
            });
        }
    }
})();
