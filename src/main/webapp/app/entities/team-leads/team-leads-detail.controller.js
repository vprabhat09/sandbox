(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TeamLeadsDetailController', TeamLeadsDetailController);

    TeamLeadsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TeamLeads', 'Employee', 'SubTeam'];

    function TeamLeadsDetailController($scope, $rootScope, $stateParams, previousState, entity, TeamLeads, Employee, SubTeam) {
        var vm = this;

        vm.teamLeads = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('modulemanagementV3App:teamLeadsUpdate', function(event, result) {
            vm.teamLeads = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
