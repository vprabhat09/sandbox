(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TeamLeadsDeleteController',TeamLeadsDeleteController);

    TeamLeadsDeleteController.$inject = ['$uibModalInstance', 'entity', 'TeamLeads'];

    function TeamLeadsDeleteController($uibModalInstance, entity, TeamLeads) {
        var vm = this;

        vm.teamLeads = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TeamLeads.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
