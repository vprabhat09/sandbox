(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TeamLeadsDialogController', TeamLeadsDialogController);

    TeamLeadsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'TeamLeads', 'Employee', 'SubTeam'];

    function TeamLeadsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, TeamLeads, Employee, SubTeam) {
        var vm = this;

        vm.teamLeads = entity;
        vm.clear = clear;
        vm.save = save;
        vm.employees = Employee.query({filter: 'teamlead-is-null'});
        $q.all([vm.teamLeads.$promise, vm.employees.$promise]).then(function() {
            if (!vm.teamLeads.employeeId) {
                return $q.reject();
            }
            return Employee.get({id : vm.teamLeads.employeeId}).$promise;
        }).then(function(employee) {
            vm.employees.push(employee);
        });
        vm.teams = SubTeam.query({filter: 'teamleads-is-null'});
        $q.all([vm.teamLeads.$promise, vm.teams.$promise]).then(function() {
            if (!vm.teamLeads.teamId) {
                return $q.reject();
            }
            return SubTeam.get({id : vm.teamLeads.teamId}).$promise;
        }).then(function(team) {
            vm.teams.push(team);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.teamLeads.id !== null) {
                TeamLeads.update(vm.teamLeads, onSaveSuccess, onSaveError);
            } else {
                TeamLeads.save(vm.teamLeads, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('modulemanagementV3App:teamLeadsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
