(function() {
    'use strict';
    angular
        .module('modulemanagementV3App')
        .factory('TeamLeads', TeamLeads);

    TeamLeads.$inject = ['$resource'];

    function TeamLeads ($resource) {
        var resourceUrl =  'api/team-leads/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
