(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TeamDetailController', TeamDetailController);

    TeamDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Team', 'SubTeam', 'Project'];

    function TeamDetailController($scope, $rootScope, $stateParams, previousState, entity, Team, SubTeam, Project) {
        var vm = this;

        vm.team = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('modulemanagementV3App:teamUpdate', function(event, result) {
            vm.team = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
