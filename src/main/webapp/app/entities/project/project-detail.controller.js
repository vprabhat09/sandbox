(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('ProjectDetailController', ProjectDetailController);

    ProjectDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Project', 'Team', 'Tcsaccount'];

    function ProjectDetailController($scope, $rootScope, $stateParams, previousState, entity, Project, Team, Tcsaccount) {
        var vm = this;

        vm.project = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('modulemanagementV3App:projectUpdate', function(event, result) {
            vm.project = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
