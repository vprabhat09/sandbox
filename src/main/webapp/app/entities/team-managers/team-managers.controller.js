(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TeamManagersController', TeamManagersController);

    TeamManagersController.$inject = ['$scope', '$state', 'TeamManagers'];

    function TeamManagersController ($scope, $state, TeamManagers) {
        var vm = this;
        
        vm.teamManagers = [];

        loadAll();

        function loadAll() {
            TeamManagers.query(function(result) {
                vm.teamManagers = result;
            });
        }
    }
})();
