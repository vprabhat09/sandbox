(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TeamManagersDetailController', TeamManagersDetailController);

    TeamManagersDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TeamManagers', 'Employee', 'Team'];

    function TeamManagersDetailController($scope, $rootScope, $stateParams, previousState, entity, TeamManagers, Employee, Team) {
        var vm = this;

        vm.teamManagers = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('modulemanagementV3App:teamManagersUpdate', function(event, result) {
            vm.teamManagers = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
