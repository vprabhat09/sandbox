(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TeamManagersDialogController', TeamManagersDialogController);

    TeamManagersDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'TeamManagers', 'Employee', 'Team'];

    function TeamManagersDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, TeamManagers, Employee, Team) {
        var vm = this;

        vm.teamManagers = entity;
        vm.clear = clear;
        vm.save = save;
        vm.employees = Employee.query({filter: 'teammanger-is-null'});
        $q.all([vm.teamManagers.$promise, vm.employees.$promise]).then(function() {
            if (!vm.teamManagers.employeeId) {
                return $q.reject();
            }
            return Employee.get({id : vm.teamManagers.employeeId}).$promise;
        }).then(function(employee) {
            vm.employees.push(employee);
        });
        vm.teams = Team.query({filter: 'teammanagers-is-null'});
        $q.all([vm.teamManagers.$promise, vm.teams.$promise]).then(function() {
            if (!vm.teamManagers.teamId) {
                return $q.reject();
            }
            return Team.get({id : vm.teamManagers.teamId}).$promise;
        }).then(function(team) {
            vm.teams.push(team);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.teamManagers.id !== null) {
                TeamManagers.update(vm.teamManagers, onSaveSuccess, onSaveError);
            } else {
                TeamManagers.save(vm.teamManagers, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('modulemanagementV3App:teamManagersUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
