(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TeamManagersDeleteController',TeamManagersDeleteController);

    TeamManagersDeleteController.$inject = ['$uibModalInstance', 'entity', 'TeamManagers'];

    function TeamManagersDeleteController($uibModalInstance, entity, TeamManagers) {
        var vm = this;

        vm.teamManagers = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TeamManagers.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
