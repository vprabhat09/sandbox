(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('team-managers', {
            parent: 'entity',
            url: '/team-managers',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'TeamManagers'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/team-managers/team-managers.html',
                    controller: 'TeamManagersController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('team-managers-detail', {
            parent: 'entity',
            url: '/team-managers/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'TeamManagers'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/team-managers/team-managers-detail.html',
                    controller: 'TeamManagersDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'TeamManagers', function($stateParams, TeamManagers) {
                    return TeamManagers.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'team-managers',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('team-managers-detail.edit', {
            parent: 'team-managers-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/team-managers/team-managers-dialog.html',
                    controller: 'TeamManagersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TeamManagers', function(TeamManagers) {
                            return TeamManagers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('team-managers.new', {
            parent: 'team-managers',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/team-managers/team-managers-dialog.html',
                    controller: 'TeamManagersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                teamManagerId: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('team-managers', null, { reload: true });
                }, function() {
                    $state.go('team-managers');
                });
            }]
        })
        .state('team-managers.edit', {
            parent: 'team-managers',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/team-managers/team-managers-dialog.html',
                    controller: 'TeamManagersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TeamManagers', function(TeamManagers) {
                            return TeamManagers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('team-managers', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('team-managers.delete', {
            parent: 'team-managers',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/team-managers/team-managers-delete-dialog.html',
                    controller: 'TeamManagersDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TeamManagers', function(TeamManagers) {
                            return TeamManagers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('team-managers', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
