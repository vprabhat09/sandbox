(function() {
    'use strict';
    angular
        .module('modulemanagementV3App')
        .factory('TeamManagers', TeamManagers);

    TeamManagers.$inject = ['$resource'];

    function TeamManagers ($resource) {
        var resourceUrl =  'api/team-managers/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
