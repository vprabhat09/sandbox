(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TcsaccountDetailController', TcsaccountDetailController);

    TcsaccountDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Tcsaccount', 'Project'];

    function TcsaccountDetailController($scope, $rootScope, $stateParams, previousState, entity, Tcsaccount, Project) {
        var vm = this;

        vm.tcsaccount = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('modulemanagementV3App:tcsaccountUpdate', function(event, result) {
            vm.tcsaccount = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
