(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TcsaccountController', TcsaccountController);

    TcsaccountController.$inject = ['$scope', '$state', 'Tcsaccount'];

    function TcsaccountController ($scope, $state, Tcsaccount) {
        var vm = this;
        
        vm.tcsaccounts = [];

        loadAll();

        function loadAll() {
            Tcsaccount.query(function(result) {
                vm.tcsaccounts = result;
            });
        }
    }
})();
