(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TcsaccountDialogController', TcsaccountDialogController);

    TcsaccountDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Tcsaccount', 'Project'];

    function TcsaccountDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Tcsaccount, Project) {
        var vm = this;

        vm.tcsaccount = entity;
        vm.clear = clear;
        vm.save = save;
        vm.projects = Project.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.tcsaccount.id !== null) {
                Tcsaccount.update(vm.tcsaccount, onSaveSuccess, onSaveError);
            } else {
                Tcsaccount.save(vm.tcsaccount, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('modulemanagementV3App:tcsaccountUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
