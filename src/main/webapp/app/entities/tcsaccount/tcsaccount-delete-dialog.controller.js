(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TcsaccountDeleteController',TcsaccountDeleteController);

    TcsaccountDeleteController.$inject = ['$uibModalInstance', 'entity', 'Tcsaccount'];

    function TcsaccountDeleteController($uibModalInstance, entity, Tcsaccount) {
        var vm = this;

        vm.tcsaccount = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Tcsaccount.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
