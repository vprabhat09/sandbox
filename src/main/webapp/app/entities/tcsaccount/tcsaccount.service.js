(function() {
    'use strict';
    angular
        .module('modulemanagementV3App')
        .factory('Tcsaccount', Tcsaccount);

    Tcsaccount.$inject = ['$resource'];

    function Tcsaccount ($resource) {
        var resourceUrl =  'api/tcsaccounts/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
