(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('tcsaccount', {
            parent: 'entity',
            url: '/tcsaccount',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Tcsaccounts'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/tcsaccount/tcsaccounts.html',
                    controller: 'TcsaccountController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('tcsaccount-detail', {
            parent: 'entity',
            url: '/tcsaccount/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Tcsaccount'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/tcsaccount/tcsaccount-detail.html',
                    controller: 'TcsaccountDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Tcsaccount', function($stateParams, Tcsaccount) {
                    return Tcsaccount.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'tcsaccount',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('tcsaccount-detail.edit', {
            parent: 'tcsaccount-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tcsaccount/tcsaccount-dialog.html',
                    controller: 'TcsaccountDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Tcsaccount', function(Tcsaccount) {
                            return Tcsaccount.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('tcsaccount.new', {
            parent: 'tcsaccount',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tcsaccount/tcsaccount-dialog.html',
                    controller: 'TcsaccountDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                accountId: null,
                                name: null,
                                client: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('tcsaccount', null, { reload: true });
                }, function() {
                    $state.go('tcsaccount');
                });
            }]
        })
        .state('tcsaccount.edit', {
            parent: 'tcsaccount',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tcsaccount/tcsaccount-dialog.html',
                    controller: 'TcsaccountDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Tcsaccount', function(Tcsaccount) {
                            return Tcsaccount.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('tcsaccount', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('tcsaccount.delete', {
            parent: 'tcsaccount',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tcsaccount/tcsaccount-delete-dialog.html',
                    controller: 'TcsaccountDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Tcsaccount', function(Tcsaccount) {
                            return Tcsaccount.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('tcsaccount', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
