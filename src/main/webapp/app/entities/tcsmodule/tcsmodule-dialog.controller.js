(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TcsmoduleDialogController', TcsmoduleDialogController);

    TcsmoduleDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Tcsmodule', 'Seat'];

    function TcsmoduleDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Tcsmodule, Seat) {
        var vm = this;

        vm.tcsmodule = entity;
        vm.clear = clear;
        vm.save = save;
        vm.seats = Seat.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.tcsmodule.id !== null) {
                Tcsmodule.update(vm.tcsmodule, onSaveSuccess, onSaveError);
            } else {
                Tcsmodule.save(vm.tcsmodule, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('modulemanagementV3App:tcsmoduleUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
