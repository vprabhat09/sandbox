(function() {
    'use strict';
    angular
        .module('modulemanagementV3App')
        .factory('Tcsmodule', Tcsmodule);

    Tcsmodule.$inject = ['$resource'];

    function Tcsmodule ($resource) {
        var resourceUrl =  'api/tcsmodules/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
