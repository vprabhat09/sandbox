(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TcsmoduleController', TcsmoduleController);

    TcsmoduleController.$inject = ['$scope', '$state', 'Tcsmodule'];

    function TcsmoduleController ($scope, $state, Tcsmodule) {
        var vm = this;
        
        vm.tcsmodules = [];

        loadAll();

        function loadAll() {
            Tcsmodule.query(function(result) {
                vm.tcsmodules = result;
            });
        }
    }
})();
