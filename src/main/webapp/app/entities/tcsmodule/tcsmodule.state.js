(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('tcsmodule', {
            parent: 'entity',
            url: '/tcsmodule',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Tcsmodules'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/tcsmodule/tcsmodules.html',
                    controller: 'TcsmoduleController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('tcsmodule-detail', {
            parent: 'entity',
            url: '/tcsmodule/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Tcsmodule'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/tcsmodule/tcsmodule-detail.html',
                    controller: 'TcsmoduleDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Tcsmodule', function($stateParams, Tcsmodule) {
                    return Tcsmodule.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'tcsmodule',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('tcsmodule-detail.edit', {
            parent: 'tcsmodule-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tcsmodule/tcsmodule-dialog.html',
                    controller: 'TcsmoduleDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Tcsmodule', function(Tcsmodule) {
                            return Tcsmodule.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('tcsmodule.new', {
            parent: 'tcsmodule',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tcsmodule/tcsmodule-dialog.html',
                    controller: 'TcsmoduleDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                moduleId: null,
                                wing: null,
                                blockName: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('tcsmodule', null, { reload: true });
                }, function() {
                    $state.go('tcsmodule');
                });
            }]
        })
        .state('tcsmodule.edit', {
            parent: 'tcsmodule',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tcsmodule/tcsmodule-dialog.html',
                    controller: 'TcsmoduleDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Tcsmodule', function(Tcsmodule) {
                            return Tcsmodule.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('tcsmodule', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('tcsmodule.delete', {
            parent: 'tcsmodule',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tcsmodule/tcsmodule-delete-dialog.html',
                    controller: 'TcsmoduleDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Tcsmodule', function(Tcsmodule) {
                            return Tcsmodule.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('tcsmodule', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
