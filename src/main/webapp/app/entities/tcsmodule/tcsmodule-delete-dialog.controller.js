(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TcsmoduleDeleteController',TcsmoduleDeleteController);

    TcsmoduleDeleteController.$inject = ['$uibModalInstance', 'entity', 'Tcsmodule'];

    function TcsmoduleDeleteController($uibModalInstance, entity, Tcsmodule) {
        var vm = this;

        vm.tcsmodule = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Tcsmodule.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
