(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TcsmoduleDetailController', TcsmoduleDetailController);

    TcsmoduleDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Tcsmodule', 'Seat'];

    function TcsmoduleDetailController($scope, $rootScope, $stateParams, previousState, entity, Tcsmodule, Seat) {
        var vm = this;

        vm.tcsmodule = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('modulemanagementV3App:tcsmoduleUpdate', function(event, result) {
            vm.tcsmodule = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
