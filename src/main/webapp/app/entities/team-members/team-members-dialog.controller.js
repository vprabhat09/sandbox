(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TeamMembersDialogController', TeamMembersDialogController);

    TeamMembersDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'TeamMembers', 'SubTeam', 'Employee'];

    function TeamMembersDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, TeamMembers, SubTeam, Employee) {
        var vm = this;

        vm.teamMembers = entity;
        vm.clear = clear;
        vm.save = save;
        vm.subteams = SubTeam.query({filter: 'teammembers-is-null'});
        $q.all([vm.teamMembers.$promise, vm.subteams.$promise]).then(function() {
            if (!vm.teamMembers.subTeamId) {
                return $q.reject();
            }
            return SubTeam.get({id : vm.teamMembers.subTeamId}).$promise;
        }).then(function(subTeam) {
            vm.subteams.push(subTeam);
        });
        vm.employees = Employee.query({filter: 'teammembers-is-null'});
        $q.all([vm.teamMembers.$promise, vm.employees.$promise]).then(function() {
            if (!vm.teamMembers.employeeId) {
                return $q.reject();
            }
            return Employee.get({id : vm.teamMembers.employeeId}).$promise;
        }).then(function(employee) {
            vm.employees.push(employee);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.teamMembers.id !== null) {
                TeamMembers.update(vm.teamMembers, onSaveSuccess, onSaveError);
            } else {
                TeamMembers.save(vm.teamMembers, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('modulemanagementV3App:teamMembersUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
