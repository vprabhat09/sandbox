(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TeamMembersController', TeamMembersController);

    TeamMembersController.$inject = ['$scope', '$state', 'TeamMembers'];

    function TeamMembersController ($scope, $state, TeamMembers) {
        var vm = this;
        
        vm.teamMembers = [];

        loadAll();

        function loadAll() {
            TeamMembers.query(function(result) {
                vm.teamMembers = result;
            });
        }
    }
})();
