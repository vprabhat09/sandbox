(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('team-members', {
            parent: 'entity',
            url: '/team-members',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'TeamMembers'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/team-members/team-members.html',
                    controller: 'TeamMembersController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('team-members-detail', {
            parent: 'entity',
            url: '/team-members/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'TeamMembers'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/team-members/team-members-detail.html',
                    controller: 'TeamMembersDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'TeamMembers', function($stateParams, TeamMembers) {
                    return TeamMembers.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'team-members',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('team-members-detail.edit', {
            parent: 'team-members-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/team-members/team-members-dialog.html',
                    controller: 'TeamMembersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TeamMembers', function(TeamMembers) {
                            return TeamMembers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('team-members.new', {
            parent: 'team-members',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/team-members/team-members-dialog.html',
                    controller: 'TeamMembersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                memberId: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('team-members', null, { reload: true });
                }, function() {
                    $state.go('team-members');
                });
            }]
        })
        .state('team-members.edit', {
            parent: 'team-members',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/team-members/team-members-dialog.html',
                    controller: 'TeamMembersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TeamMembers', function(TeamMembers) {
                            return TeamMembers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('team-members', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('team-members.delete', {
            parent: 'team-members',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/team-members/team-members-delete-dialog.html',
                    controller: 'TeamMembersDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TeamMembers', function(TeamMembers) {
                            return TeamMembers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('team-members', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
