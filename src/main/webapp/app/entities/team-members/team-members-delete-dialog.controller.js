(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TeamMembersDeleteController',TeamMembersDeleteController);

    TeamMembersDeleteController.$inject = ['$uibModalInstance', 'entity', 'TeamMembers'];

    function TeamMembersDeleteController($uibModalInstance, entity, TeamMembers) {
        var vm = this;

        vm.teamMembers = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TeamMembers.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
