(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('TeamMembersDetailController', TeamMembersDetailController);

    TeamMembersDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TeamMembers', 'SubTeam', 'Employee'];

    function TeamMembersDetailController($scope, $rootScope, $stateParams, previousState, entity, TeamMembers, SubTeam, Employee) {
        var vm = this;

        vm.teamMembers = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('modulemanagementV3App:teamMembersUpdate', function(event, result) {
            vm.teamMembers = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
