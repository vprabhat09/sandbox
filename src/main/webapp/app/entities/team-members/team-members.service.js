(function() {
    'use strict';
    angular
        .module('modulemanagementV3App')
        .factory('TeamMembers', TeamMembers);

    TeamMembers.$inject = ['$resource'];

    function TeamMembers ($resource) {
        var resourceUrl =  'api/team-members/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
