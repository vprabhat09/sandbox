(function() {
    'use strict';
    angular
        .module('modulemanagementV3App')
        .factory('AccountManagers', AccountManagers);

    AccountManagers.$inject = ['$resource'];

    function AccountManagers ($resource) {
        var resourceUrl =  'api/account-managers/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
