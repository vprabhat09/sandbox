(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('AccountManagersController', AccountManagersController);

    AccountManagersController.$inject = ['$scope', '$state', 'AccountManagers'];

    function AccountManagersController ($scope, $state, AccountManagers) {
        var vm = this;
        
        vm.accountManagers = [];

        loadAll();

        function loadAll() {
            AccountManagers.query(function(result) {
                vm.accountManagers = result;
            });
        }
    }
})();
