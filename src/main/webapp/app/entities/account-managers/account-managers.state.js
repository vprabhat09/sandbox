(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('account-managers', {
            parent: 'entity',
            url: '/account-managers',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'AccountManagers'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/account-managers/account-managers.html',
                    controller: 'AccountManagersController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('account-managers-detail', {
            parent: 'entity',
            url: '/account-managers/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'AccountManagers'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/account-managers/account-managers-detail.html',
                    controller: 'AccountManagersDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'AccountManagers', function($stateParams, AccountManagers) {
                    return AccountManagers.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'account-managers',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('account-managers-detail.edit', {
            parent: 'account-managers-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/account-managers/account-managers-dialog.html',
                    controller: 'AccountManagersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AccountManagers', function(AccountManagers) {
                            return AccountManagers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('account-managers.new', {
            parent: 'account-managers',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/account-managers/account-managers-dialog.html',
                    controller: 'AccountManagersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                managerId: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('account-managers', null, { reload: true });
                }, function() {
                    $state.go('account-managers');
                });
            }]
        })
        .state('account-managers.edit', {
            parent: 'account-managers',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/account-managers/account-managers-dialog.html',
                    controller: 'AccountManagersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AccountManagers', function(AccountManagers) {
                            return AccountManagers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('account-managers', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('account-managers.delete', {
            parent: 'account-managers',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/account-managers/account-managers-delete-dialog.html',
                    controller: 'AccountManagersDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AccountManagers', function(AccountManagers) {
                            return AccountManagers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('account-managers', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
