(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('AccountManagersDialogController', AccountManagersDialogController);

    AccountManagersDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'AccountManagers', 'Tcsaccount', 'Employee'];

    function AccountManagersDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, AccountManagers, Tcsaccount, Employee) {
        var vm = this;

        vm.accountManagers = entity;
        vm.clear = clear;
        vm.save = save;
        vm.tcsaccounts = Tcsaccount.query({filter: 'accountmanagers-is-null'});
        $q.all([vm.accountManagers.$promise, vm.tcsaccounts.$promise]).then(function() {
            if (!vm.accountManagers.tcsAccountId) {
                return $q.reject();
            }
            return Tcsaccount.get({id : vm.accountManagers.tcsAccountId}).$promise;
        }).then(function(tcsAccount) {
            vm.tcsaccounts.push(tcsAccount);
        });
        vm.employees = Employee.query({filter: 'accountmanager-is-null'});
        $q.all([vm.accountManagers.$promise, vm.employees.$promise]).then(function() {
            if (!vm.accountManagers.employeeId) {
                return $q.reject();
            }
            return Employee.get({id : vm.accountManagers.employeeId}).$promise;
        }).then(function(employee) {
            vm.employees.push(employee);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.accountManagers.id !== null) {
                AccountManagers.update(vm.accountManagers, onSaveSuccess, onSaveError);
            } else {
                AccountManagers.save(vm.accountManagers, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('modulemanagementV3App:accountManagersUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
