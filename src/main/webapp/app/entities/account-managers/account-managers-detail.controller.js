(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('AccountManagersDetailController', AccountManagersDetailController);

    AccountManagersDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'AccountManagers', 'Tcsaccount', 'Employee'];

    function AccountManagersDetailController($scope, $rootScope, $stateParams, previousState, entity, AccountManagers, Tcsaccount, Employee) {
        var vm = this;

        vm.accountManagers = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('modulemanagementV3App:accountManagersUpdate', function(event, result) {
            vm.accountManagers = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
