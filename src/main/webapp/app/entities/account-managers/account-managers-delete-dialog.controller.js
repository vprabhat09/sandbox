(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('AccountManagersDeleteController',AccountManagersDeleteController);

    AccountManagersDeleteController.$inject = ['$uibModalInstance', 'entity', 'AccountManagers'];

    function AccountManagersDeleteController($uibModalInstance, entity, AccountManagers) {
        var vm = this;

        vm.accountManagers = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            AccountManagers.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
