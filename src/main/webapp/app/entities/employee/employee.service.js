(function() {
    'use strict';
    angular
        .module('modulemanagementV3App')
        .factory('Employee', Employee);
    angular
        .module('modulemanagementV3App')
        .factory('CustomEmployee', CustomEmployee);

    Employee.$inject = ['$resource', 'DateUtils'];
    CustomEmployee.$inject = ['$resource', 'DateUtils'];
    function CustomEmployee ($resource, DateUtils) {
        var resourceUrl =  'api/employeeById/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.joiningDate = DateUtils.convertLocalDateFromServer(data.joiningDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.joiningDate = DateUtils.convertLocalDateToServer(data.joiningDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.joiningDate = DateUtils.convertLocalDateToServer(data.joiningDate);
                    return angular.toJson(data);
                }
            }
        });
    }
    function Employee ($resource, DateUtils) {
        var resourceUrl =  'api/employees/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.joiningDate = DateUtils.convertLocalDateFromServer(data.joiningDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.joiningDate = DateUtils.convertLocalDateToServer(data.joiningDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.joiningDate = DateUtils.convertLocalDateToServer(data.joiningDate);
                    return angular.toJson(data);
                }
            }
        });
    }
})();
