(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('EmployeeDetailController', EmployeeDetailController);

    EmployeeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Employee', 'AccountManagers', 'TeamManagers', 'ProjectManagers', 'TeamLeads', 'TeamMembers', 'Seat'];

    function EmployeeDetailController($scope, $rootScope, $stateParams, previousState, entity, Employee, AccountManagers, TeamManagers, ProjectManagers, TeamLeads, TeamMembers, Seat) {
        var vm = this;

        vm.employee = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('modulemanagementV3App:employeeUpdate', function(event, result) {
            vm.employee = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
