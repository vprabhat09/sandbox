(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .controller('EmployeeSearchController', EmployeeSearchController);

    EmployeeSearchController.$inject = ['$scope', '$rootScope', '$stateParams','CustomEmployee', 'Employee', 'AccountManagers', 'TeamManagers', 'ProjectManagers', 'TeamLeads', 'TeamMembers', 'Seat'];

    function EmployeeSearchController($scope, $rootScope, $stateParams,CustomEmployee, Employee, AccountManagers, TeamManagers, ProjectManagers, TeamLeads, TeamMembers, Seat) {
        var vm = this;
         vm.empId = "";
        vm.employee = null;

        vm.searchEmployee = searchEmployee;


         function searchEmployee(empId){

            var employeeDetails= CustomEmployee.get({id : vm.empId},function (){
                console.log(employeeDetails)
                 vm.employee = employeeDetails;
             });

        }
    }
})();
