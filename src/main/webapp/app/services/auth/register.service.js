(function () {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .factory('Register', Register);

    Register.$inject = ['$resource'];

    function Register ($resource) {
        return $resource('api/register', {}, {});
    }
})();
