(function() {
    'use strict';

    angular
        .module('modulemanagementV3App')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
