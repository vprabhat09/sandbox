package com.tcs.modulemanagement.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Tcsaccount.
 */
@Entity
@Table(name = "tcsaccount")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Tcsaccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "account_id")
    private Integer accountId;

    @Column(name = "name")
    private String name;

    @Column(name = "client")
    private String client;

    @OneToMany(mappedBy = "tcsaccount")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Project> tcsProjects = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public Set<Project> getTcsProjects() {
        return tcsProjects;
    }

    public void setTcsProjects(Set<Project> projects) {
        this.tcsProjects = projects;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tcsaccount tcsaccount = (Tcsaccount) o;
        if(tcsaccount.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, tcsaccount.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Tcsaccount{" +
            "id=" + id +
            ", accountId='" + accountId + "'" +
            ", name='" + name + "'" +
            ", client='" + client + "'" +
            '}';
    }
}
