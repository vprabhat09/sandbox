package com.tcs.modulemanagement.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

import com.tcs.modulemanagement.domain.enumeration.Role;

/**
 * A Employee.
 */
@Entity
@Table(name = "employee")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "emp_id")
    private Integer empId;

    @Column(name = "name")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Role role;

    @Column(name = "address")
    private String address;

    @Column(name = "contact_no", precision=10, scale=2)
    private BigDecimal contactNo;

    @Column(name = "extn_number")
    private Integer extnNumber;

    @Column(name = "joining_date")
    private LocalDate joiningDate;

    @OneToOne(mappedBy = "employee")
    @JsonIgnore
    private AccountManagers accountManager;

    @OneToOne(mappedBy = "employee")
    @JsonIgnore
    private TeamManagers teamManger;

    @OneToOne(mappedBy = "employee")
    @JsonIgnore
    private ProjectManagers projectManager;

    @OneToOne(mappedBy = "employee")
    @JsonIgnore
    private TeamLeads teamLead;

    @OneToOne(mappedBy = "employee")
    @JsonIgnore
    private TeamMembers teamMembers;

    @OneToOne(mappedBy = "employee")
    @JsonIgnore
    private Seat seat;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getEmpId() {
        return empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getContactNo() {
        return contactNo;
    }

    public void setContactNo(BigDecimal contactNo) {
        this.contactNo = contactNo;
    }

    public Integer getExtnNumber() {
        return extnNumber;
    }

    public void setExtnNumber(Integer extnNumber) {
        this.extnNumber = extnNumber;
    }

    public LocalDate getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(LocalDate joiningDate) {
        this.joiningDate = joiningDate;
    }

    public AccountManagers getAccountManager() {
        return accountManager;
    }

    public void setAccountManager(AccountManagers accountManagers) {
        this.accountManager = accountManagers;
    }

    public TeamManagers getTeamManger() {
        return teamManger;
    }

    public void setTeamManger(TeamManagers teamManagers) {
        this.teamManger = teamManagers;
    }

    public ProjectManagers getProjectManager() {
        return projectManager;
    }

    public void setProjectManager(ProjectManagers projectManagers) {
        this.projectManager = projectManagers;
    }

    public TeamLeads getTeamLead() {
        return teamLead;
    }

    public void setTeamLead(TeamLeads teamLeads) {
        this.teamLead = teamLeads;
    }

    public TeamMembers getTeamMembers() {
        return teamMembers;
    }

    public void setTeamMembers(TeamMembers teamMembers) {
        this.teamMembers = teamMembers;
    }

    public Seat getSeat() {
        return seat;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Employee employee = (Employee) o;
        if(employee.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, employee.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Employee{" +
            "id=" + id +
            ", empId='" + empId + "'" +
            ", name='" + name + "'" +
            ", role='" + role + "'" +
            ", address='" + address + "'" +
            ", contactNo='" + contactNo + "'" +
            ", extnNumber='" + extnNumber + "'" +
            ", joiningDate='" + joiningDate + "'" +
            '}';
    }
}
