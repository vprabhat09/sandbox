package com.tcs.modulemanagement.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A AccountManagers.
 */
@Entity
@Table(name = "account_managers")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AccountManagers implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "manager_id")
    private Integer managerId;

    @OneToOne
    @JoinColumn(unique = true)
    private Tcsaccount tcsAccount;

    @OneToOne
    @JoinColumn(unique = true)
    private Employee employee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getManagerId() {
        return managerId;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }

    public Tcsaccount getTcsAccount() {
        return tcsAccount;
    }

    public void setTcsAccount(Tcsaccount tcsaccount) {
        this.tcsAccount = tcsaccount;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccountManagers accountManagers = (AccountManagers) o;
        if(accountManagers.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, accountManagers.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "AccountManagers{" +
            "id=" + id +
            ", managerId='" + managerId + "'" +
            '}';
    }
}
