package com.tcs.modulemanagement.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A SubTeam.
 */
@Entity
@Table(name = "sub_team")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SubTeam implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "sub_team_id")
    private Integer subTeamId;

    @Column(name = "sub_team_name")
    private String subTeamName;

    @ManyToOne
    private Team team;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSubTeamId() {
        return subTeamId;
    }

    public void setSubTeamId(Integer subTeamId) {
        this.subTeamId = subTeamId;
    }

    public String getSubTeamName() {
        return subTeamName;
    }

    public void setSubTeamName(String subTeamName) {
        this.subTeamName = subTeamName;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SubTeam subTeam = (SubTeam) o;
        if(subTeam.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, subTeam.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SubTeam{" +
            "id=" + id +
            ", subTeamId='" + subTeamId + "'" +
            ", subTeamName='" + subTeamName + "'" +
            '}';
    }
}
