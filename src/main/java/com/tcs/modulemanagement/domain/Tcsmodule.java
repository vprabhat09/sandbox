package com.tcs.modulemanagement.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.tcs.modulemanagement.domain.enumeration.Wing;

import com.tcs.modulemanagement.domain.enumeration.BlockName;

/**
 * A Tcsmodule.
 */
@Entity
@Table(name = "tcsmodule")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Tcsmodule implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "module_id")
    private Integer moduleId;

    @Enumerated(EnumType.STRING)
    @Column(name = "wing")
    private Wing wing;

    @Enumerated(EnumType.STRING)
    @Column(name = "block_name")
    private BlockName blockName;

    @OneToMany(mappedBy = "module")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Seat> seats = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public Wing getWing() {
        return wing;
    }

    public void setWing(Wing wing) {
        this.wing = wing;
    }

    public BlockName getBlockName() {
        return blockName;
    }

    public void setBlockName(BlockName blockName) {
        this.blockName = blockName;
    }

    public Set<Seat> getSeats() {
        return seats;
    }

    public void setSeats(Set<Seat> seats) {
        this.seats = seats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tcsmodule tcsmodule = (Tcsmodule) o;
        if(tcsmodule.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, tcsmodule.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Tcsmodule{" +
            "id=" + id +
            ", moduleId='" + moduleId + "'" +
            ", wing='" + wing + "'" +
            ", blockName='" + blockName + "'" +
            '}';
    }
}
