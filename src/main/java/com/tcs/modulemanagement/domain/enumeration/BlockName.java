package com.tcs.modulemanagement.domain.enumeration;

/**
 * The BlockName enumeration.
 */
public enum BlockName {
    EB1,EB2,EB3,EB4,EB5,EB6
}
