package com.tcs.modulemanagement.domain.enumeration;

/**
 * The Wing enumeration.
 */
public enum Wing {
    NW,SW
}
