package com.tcs.modulemanagement.domain.enumeration;

/**
 * The Role enumeration.
 */
public enum Role {
    ACCOUNTMANAGER,TEAMLEAD,PROJECTMANAGER,TEAMMANAGER,TEAMMEMBER
}
