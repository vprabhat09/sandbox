package com.tcs.modulemanagement.repository;

import com.tcs.modulemanagement.domain.TeamLeads;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TeamLeads entity.
 */
@SuppressWarnings("unused")
public interface TeamLeadsRepository extends JpaRepository<TeamLeads,Long> {

}
