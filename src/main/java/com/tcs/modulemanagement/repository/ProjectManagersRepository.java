package com.tcs.modulemanagement.repository;

import com.tcs.modulemanagement.domain.ProjectManagers;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ProjectManagers entity.
 */
@SuppressWarnings("unused")
public interface ProjectManagersRepository extends JpaRepository<ProjectManagers,Long> {

}
