package com.tcs.modulemanagement.repository;

import com.tcs.modulemanagement.domain.Seat;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Seat entity.
 */
@SuppressWarnings("unused")
public interface SeatRepository extends JpaRepository<Seat,Long> {

}
