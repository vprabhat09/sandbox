package com.tcs.modulemanagement.repository;

import com.tcs.modulemanagement.domain.TeamMembers;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TeamMembers entity.
 */
@SuppressWarnings("unused")
public interface TeamMembersRepository extends JpaRepository<TeamMembers,Long> {

}
