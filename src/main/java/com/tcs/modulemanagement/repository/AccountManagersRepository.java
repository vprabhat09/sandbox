package com.tcs.modulemanagement.repository;

import com.tcs.modulemanagement.domain.AccountManagers;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the AccountManagers entity.
 */
@SuppressWarnings("unused")
public interface AccountManagersRepository extends JpaRepository<AccountManagers,Long> {

}
