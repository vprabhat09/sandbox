package com.tcs.modulemanagement.repository;

import com.tcs.modulemanagement.domain.Tcsaccount;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Tcsaccount entity.
 */
@SuppressWarnings("unused")
public interface TcsaccountRepository extends JpaRepository<Tcsaccount,Long> {

}
