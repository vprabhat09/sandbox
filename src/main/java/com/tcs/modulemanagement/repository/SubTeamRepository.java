package com.tcs.modulemanagement.repository;

import com.tcs.modulemanagement.domain.SubTeam;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the SubTeam entity.
 */
@SuppressWarnings("unused")
public interface SubTeamRepository extends JpaRepository<SubTeam,Long> {

}
