package com.tcs.modulemanagement.repository;

import com.tcs.modulemanagement.domain.Tcsmodule;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Tcsmodule entity.
 */
@SuppressWarnings("unused")
public interface TcsmoduleRepository extends JpaRepository<Tcsmodule,Long> {

}
