package com.tcs.modulemanagement.repository;

import com.tcs.modulemanagement.domain.TeamManagers;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TeamManagers entity.
 */
@SuppressWarnings("unused")
public interface TeamManagersRepository extends JpaRepository<TeamManagers,Long> {

}
