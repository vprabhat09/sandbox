package com.tcs.modulemanagement.repository;

import com.tcs.modulemanagement.domain.Employee;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Employee entity.
 */
@SuppressWarnings("unused")
public interface EmployeeRepository extends JpaRepository<Employee,Long> {


    public Employee findByEmpId(int id);
}
