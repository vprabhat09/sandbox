package com.tcs.modulemanagement.web.rest.mapper;

import com.tcs.modulemanagement.domain.*;
import com.tcs.modulemanagement.web.rest.dto.SubTeamDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity SubTeam and its DTO SubTeamDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SubTeamMapper {

    @Mapping(source = "team.id", target = "teamId")
    SubTeamDTO subTeamToSubTeamDTO(SubTeam subTeam);

    List<SubTeamDTO> subTeamsToSubTeamDTOs(List<SubTeam> subTeams);

    @Mapping(source = "teamId", target = "team")
    SubTeam subTeamDTOToSubTeam(SubTeamDTO subTeamDTO);

    List<SubTeam> subTeamDTOsToSubTeams(List<SubTeamDTO> subTeamDTOs);

    default Team teamFromId(Long id) {
        if (id == null) {
            return null;
        }
        Team team = new Team();
        team.setId(id);
        return team;
    }
}
