package com.tcs.modulemanagement.web.rest.mapper;

import com.tcs.modulemanagement.domain.*;
import com.tcs.modulemanagement.web.rest.dto.ProjectManagersDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity ProjectManagers and its DTO ProjectManagersDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProjectManagersMapper {

    @Mapping(source = "employee.id", target = "employeeId")
    @Mapping(source = "team.id", target = "teamId")
    ProjectManagersDTO projectManagersToProjectManagersDTO(ProjectManagers projectManagers);

    List<ProjectManagersDTO> projectManagersToProjectManagersDTOs(List<ProjectManagers> projectManagers);

    @Mapping(source = "employeeId", target = "employee")
    @Mapping(source = "teamId", target = "team")
    ProjectManagers projectManagersDTOToProjectManagers(ProjectManagersDTO projectManagersDTO);

    List<ProjectManagers> projectManagersDTOsToProjectManagers(List<ProjectManagersDTO> projectManagersDTOs);

    default Employee employeeFromId(Long id) {
        if (id == null) {
            return null;
        }
        Employee employee = new Employee();
        employee.setId(id);
        return employee;
    }

    default Project projectFromId(Long id) {
        if (id == null) {
            return null;
        }
        Project project = new Project();
        project.setId(id);
        return project;
    }
}
