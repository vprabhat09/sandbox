package com.tcs.modulemanagement.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.tcs.modulemanagement.domain.AccountManagers;
import com.tcs.modulemanagement.service.AccountManagersService;
import com.tcs.modulemanagement.web.rest.util.HeaderUtil;
import com.tcs.modulemanagement.web.rest.dto.AccountManagersDTO;
import com.tcs.modulemanagement.web.rest.mapper.AccountManagersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing AccountManagers.
 */
@RestController
@RequestMapping("/api")
public class AccountManagersResource {

    private final Logger log = LoggerFactory.getLogger(AccountManagersResource.class);
        
    @Inject
    private AccountManagersService accountManagersService;
    
    @Inject
    private AccountManagersMapper accountManagersMapper;
    
    /**
     * POST  /account-managers : Create a new accountManagers.
     *
     * @param accountManagersDTO the accountManagersDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new accountManagersDTO, or with status 400 (Bad Request) if the accountManagers has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/account-managers",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AccountManagersDTO> createAccountManagers(@RequestBody AccountManagersDTO accountManagersDTO) throws URISyntaxException {
        log.debug("REST request to save AccountManagers : {}", accountManagersDTO);
        if (accountManagersDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("accountManagers", "idexists", "A new accountManagers cannot already have an ID")).body(null);
        }
        AccountManagersDTO result = accountManagersService.save(accountManagersDTO);
        return ResponseEntity.created(new URI("/api/account-managers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("accountManagers", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /account-managers : Updates an existing accountManagers.
     *
     * @param accountManagersDTO the accountManagersDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated accountManagersDTO,
     * or with status 400 (Bad Request) if the accountManagersDTO is not valid,
     * or with status 500 (Internal Server Error) if the accountManagersDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/account-managers",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AccountManagersDTO> updateAccountManagers(@RequestBody AccountManagersDTO accountManagersDTO) throws URISyntaxException {
        log.debug("REST request to update AccountManagers : {}", accountManagersDTO);
        if (accountManagersDTO.getId() == null) {
            return createAccountManagers(accountManagersDTO);
        }
        AccountManagersDTO result = accountManagersService.save(accountManagersDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("accountManagers", accountManagersDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /account-managers : get all the accountManagers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of accountManagers in body
     */
    @RequestMapping(value = "/account-managers",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<AccountManagersDTO> getAllAccountManagers() {
        log.debug("REST request to get all AccountManagers");
        return accountManagersService.findAll();
    }

    /**
     * GET  /account-managers/:id : get the "id" accountManagers.
     *
     * @param id the id of the accountManagersDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the accountManagersDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/account-managers/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AccountManagersDTO> getAccountManagers(@PathVariable Long id) {
        log.debug("REST request to get AccountManagers : {}", id);
        AccountManagersDTO accountManagersDTO = accountManagersService.findOne(id);
        return Optional.ofNullable(accountManagersDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /account-managers/:id : delete the "id" accountManagers.
     *
     * @param id the id of the accountManagersDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/account-managers/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAccountManagers(@PathVariable Long id) {
        log.debug("REST request to delete AccountManagers : {}", id);
        accountManagersService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("accountManagers", id.toString())).build();
    }

}
