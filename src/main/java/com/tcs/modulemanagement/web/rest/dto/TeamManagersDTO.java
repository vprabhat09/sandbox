package com.tcs.modulemanagement.web.rest.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the TeamManagers entity.
 */
public class TeamManagersDTO implements Serializable {

    private Long id;

    private Integer teamManagerId;


    private Long employeeId;
    
    private Long teamId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Integer getTeamManagerId() {
        return teamManagerId;
    }

    public void setTeamManagerId(Integer teamManagerId) {
        this.teamManagerId = teamManagerId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TeamManagersDTO teamManagersDTO = (TeamManagersDTO) o;

        if ( ! Objects.equals(id, teamManagersDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TeamManagersDTO{" +
            "id=" + id +
            ", teamManagerId='" + teamManagerId + "'" +
            '}';
    }
}
