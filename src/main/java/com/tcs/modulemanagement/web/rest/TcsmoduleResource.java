package com.tcs.modulemanagement.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.tcs.modulemanagement.domain.Tcsmodule;
import com.tcs.modulemanagement.repository.TcsmoduleRepository;
import com.tcs.modulemanagement.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Tcsmodule.
 */
@RestController
@RequestMapping("/api")
public class TcsmoduleResource {

    private final Logger log = LoggerFactory.getLogger(TcsmoduleResource.class);
        
    @Inject
    private TcsmoduleRepository tcsmoduleRepository;
    
    /**
     * POST  /tcsmodules : Create a new tcsmodule.
     *
     * @param tcsmodule the tcsmodule to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tcsmodule, or with status 400 (Bad Request) if the tcsmodule has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/tcsmodules",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tcsmodule> createTcsmodule(@RequestBody Tcsmodule tcsmodule) throws URISyntaxException {
        log.debug("REST request to save Tcsmodule : {}", tcsmodule);
        if (tcsmodule.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tcsmodule", "idexists", "A new tcsmodule cannot already have an ID")).body(null);
        }
        Tcsmodule result = tcsmoduleRepository.save(tcsmodule);
        return ResponseEntity.created(new URI("/api/tcsmodules/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("tcsmodule", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tcsmodules : Updates an existing tcsmodule.
     *
     * @param tcsmodule the tcsmodule to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tcsmodule,
     * or with status 400 (Bad Request) if the tcsmodule is not valid,
     * or with status 500 (Internal Server Error) if the tcsmodule couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/tcsmodules",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tcsmodule> updateTcsmodule(@RequestBody Tcsmodule tcsmodule) throws URISyntaxException {
        log.debug("REST request to update Tcsmodule : {}", tcsmodule);
        if (tcsmodule.getId() == null) {
            return createTcsmodule(tcsmodule);
        }
        Tcsmodule result = tcsmoduleRepository.save(tcsmodule);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("tcsmodule", tcsmodule.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tcsmodules : get all the tcsmodules.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tcsmodules in body
     */
    @RequestMapping(value = "/tcsmodules",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Tcsmodule> getAllTcsmodules() {
        log.debug("REST request to get all Tcsmodules");
        List<Tcsmodule> tcsmodules = tcsmoduleRepository.findAll();
        return tcsmodules;
    }

    /**
     * GET  /tcsmodules/:id : get the "id" tcsmodule.
     *
     * @param id the id of the tcsmodule to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tcsmodule, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/tcsmodules/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tcsmodule> getTcsmodule(@PathVariable Long id) {
        log.debug("REST request to get Tcsmodule : {}", id);
        Tcsmodule tcsmodule = tcsmoduleRepository.findOne(id);
        return Optional.ofNullable(tcsmodule)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /tcsmodules/:id : delete the "id" tcsmodule.
     *
     * @param id the id of the tcsmodule to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/tcsmodules/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTcsmodule(@PathVariable Long id) {
        log.debug("REST request to delete Tcsmodule : {}", id);
        tcsmoduleRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tcsmodule", id.toString())).build();
    }

}
