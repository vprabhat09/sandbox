package com.tcs.modulemanagement.web.rest.dto;

import java.time.LocalDate;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.tcs.modulemanagement.domain.enumeration.Role;

/**
 * A DTO for the Employee entity.
 */
public class EmployeeDTO implements Serializable {

    private Long id;

    private Integer empId;

    private String name;

    private Role role;

    private String address;

    private BigDecimal contactNo;

    private Integer extnNumber;

    private LocalDate joiningDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Integer getEmpId() {
        return empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public BigDecimal getContactNo() {
        return contactNo;
    }

    public void setContactNo(BigDecimal contactNo) {
        this.contactNo = contactNo;
    }
    public Integer getExtnNumber() {
        return extnNumber;
    }

    public void setExtnNumber(Integer extnNumber) {
        this.extnNumber = extnNumber;
    }
    public LocalDate getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(LocalDate joiningDate) {
        this.joiningDate = joiningDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EmployeeDTO employeeDTO = (EmployeeDTO) o;

        if ( ! Objects.equals(id, employeeDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "EmployeeDTO{" +
            "id=" + id +
            ", empId='" + empId + "'" +
            ", name='" + name + "'" +
            ", role='" + role + "'" +
            ", address='" + address + "'" +
            ", contactNo='" + contactNo + "'" +
            ", extnNumber='" + extnNumber + "'" +
            ", joiningDate='" + joiningDate + "'" +
            '}';
    }
}
