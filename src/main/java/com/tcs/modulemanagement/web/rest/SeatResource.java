package com.tcs.modulemanagement.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.tcs.modulemanagement.domain.Seat;
import com.tcs.modulemanagement.repository.SeatRepository;
import com.tcs.modulemanagement.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing Seat.
 */
@RestController
@RequestMapping("/api")
public class SeatResource {

    private final Logger log = LoggerFactory.getLogger(SeatResource.class);

    @Inject
    private SeatRepository seatRepository;

    /**
     * POST  /seats : Create a new seat.
     *
     * @param seat the seat to create
     * @return the ResponseEntity with status 201 (Created) and with body the new seat, or with status 400 (Bad Request) if the seat has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/seats",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Seat> createSeat(@RequestBody Seat seat) throws URISyntaxException {
        log.debug("REST request to save Seat : {}", seat);
        if (seat.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("seat", "idexists", "A new seat cannot already have an ID")).body(null);
        }
        Seat result = seatRepository.save(seat);
        return ResponseEntity.created(new URI("/api/seats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("seat", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /seats : Updates an existing seat.
     *
     * @param seat the seat to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated seat,
     * or with status 400 (Bad Request) if the seat is not valid,
     * or with status 500 (Internal Server Error) if the seat couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/seats",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Seat> updateSeat(@RequestBody Seat seat) throws URISyntaxException {
        log.debug("REST request to update Seat : {}", seat);
        if (seat.getId() == null) {
            return createSeat(seat);
        }
        Seat result = seatRepository.save(seat);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("seat", seat.getId().toString()))
            .body(result);
    }

    /**
     * GET  /seats : get all the seats.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of seats in body
     */
    @RequestMapping(value = "/seats",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Seat> getAllSeats() {
        log.debug("REST request to get all Seats");
        List<Seat> seats = seatRepository.findAll();
        return seats;
    }

    /**
     * GET  /seats/:id : get the "id" seat.
     *
     * @param id the id of the seat to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the seat, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/seats/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Seat> getSeat(@PathVariable Long id) {
        log.debug("REST request to get Seat : {}", id);
        Seat seat = seatRepository.findOne(id);
        return Optional.ofNullable(seat)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = "/seats/getEmptySeats",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Seat> getEmptySeats() {
               return StreamSupport
            .stream(seatRepository.findAll().spliterator(), false)
            .filter(seat -> seat.getEmployee() == null)
            .collect(Collectors.toCollection(LinkedList::new));

    }

    /**
     * DELETE  /seats/:id : delete the "id" seat.
     *
     * @param id the id of the seat to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/seats/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSeat(@PathVariable Long id) {
        log.debug("REST request to delete Seat : {}", id);
        seatRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("seat", id.toString())).build();
    }

}
