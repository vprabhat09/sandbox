package com.tcs.modulemanagement.web.rest.mapper;

import com.tcs.modulemanagement.domain.*;
import com.tcs.modulemanagement.web.rest.dto.TeamLeadsDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity TeamLeads and its DTO TeamLeadsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TeamLeadsMapper {

    @Mapping(source = "employee.id", target = "employeeId")
    @Mapping(source = "team.id", target = "teamId")
    TeamLeadsDTO teamLeadsToTeamLeadsDTO(TeamLeads teamLeads);

    List<TeamLeadsDTO> teamLeadsToTeamLeadsDTOs(List<TeamLeads> teamLeads);

    @Mapping(source = "employeeId", target = "employee")
    @Mapping(source = "teamId", target = "team")
    TeamLeads teamLeadsDTOToTeamLeads(TeamLeadsDTO teamLeadsDTO);

    List<TeamLeads> teamLeadsDTOsToTeamLeads(List<TeamLeadsDTO> teamLeadsDTOs);

    default Employee employeeFromId(Long id) {
        if (id == null) {
            return null;
        }
        Employee employee = new Employee();
        employee.setId(id);
        return employee;
    }

    default SubTeam subTeamFromId(Long id) {
        if (id == null) {
            return null;
        }
        SubTeam subTeam = new SubTeam();
        subTeam.setId(id);
        return subTeam;
    }
}
