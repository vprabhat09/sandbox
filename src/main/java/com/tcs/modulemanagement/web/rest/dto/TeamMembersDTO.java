package com.tcs.modulemanagement.web.rest.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the TeamMembers entity.
 */
public class TeamMembersDTO implements Serializable {

    private Long id;

    private Integer memberId;


    private Long subTeamId;
    
    private Long employeeId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Long getSubTeamId() {
        return subTeamId;
    }

    public void setSubTeamId(Long subTeamId) {
        this.subTeamId = subTeamId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TeamMembersDTO teamMembersDTO = (TeamMembersDTO) o;

        if ( ! Objects.equals(id, teamMembersDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TeamMembersDTO{" +
            "id=" + id +
            ", memberId='" + memberId + "'" +
            '}';
    }
}
