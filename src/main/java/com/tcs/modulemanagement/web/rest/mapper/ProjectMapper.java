package com.tcs.modulemanagement.web.rest.mapper;

import com.tcs.modulemanagement.domain.*;
import com.tcs.modulemanagement.web.rest.dto.ProjectDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Project and its DTO ProjectDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProjectMapper {

    @Mapping(source = "tcsaccount.id", target = "tcsaccountId")
    ProjectDTO projectToProjectDTO(Project project);

    List<ProjectDTO> projectsToProjectDTOs(List<Project> projects);

    @Mapping(target = "teams", ignore = true)
    @Mapping(source = "tcsaccountId", target = "tcsaccount")
    Project projectDTOToProject(ProjectDTO projectDTO);

    List<Project> projectDTOsToProjects(List<ProjectDTO> projectDTOs);

    default Tcsaccount tcsaccountFromId(Long id) {
        if (id == null) {
            return null;
        }
        Tcsaccount tcsaccount = new Tcsaccount();
        tcsaccount.setId(id);
        return tcsaccount;
    }
}
