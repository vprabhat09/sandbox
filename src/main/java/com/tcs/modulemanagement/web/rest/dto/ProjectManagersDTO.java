package com.tcs.modulemanagement.web.rest.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the ProjectManagers entity.
 */
public class ProjectManagersDTO implements Serializable {

    private Long id;

    private Integer projectManagerId;


    private Long employeeId;
    
    private Long teamId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Integer getProjectManagerId() {
        return projectManagerId;
    }

    public void setProjectManagerId(Integer projectManagerId) {
        this.projectManagerId = projectManagerId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long projectId) {
        this.teamId = projectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProjectManagersDTO projectManagersDTO = (ProjectManagersDTO) o;

        if ( ! Objects.equals(id, projectManagersDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ProjectManagersDTO{" +
            "id=" + id +
            ", projectManagerId='" + projectManagerId + "'" +
            '}';
    }
}
