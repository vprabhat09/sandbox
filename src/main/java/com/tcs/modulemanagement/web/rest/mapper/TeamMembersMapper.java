package com.tcs.modulemanagement.web.rest.mapper;

import com.tcs.modulemanagement.domain.*;
import com.tcs.modulemanagement.web.rest.dto.TeamMembersDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity TeamMembers and its DTO TeamMembersDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TeamMembersMapper {

    @Mapping(source = "subTeam.id", target = "subTeamId")
    @Mapping(source = "employee.id", target = "employeeId")
    TeamMembersDTO teamMembersToTeamMembersDTO(TeamMembers teamMembers);

    List<TeamMembersDTO> teamMembersToTeamMembersDTOs(List<TeamMembers> teamMembers);

    @Mapping(source = "subTeamId", target = "subTeam")
    @Mapping(source = "employeeId", target = "employee")
    TeamMembers teamMembersDTOToTeamMembers(TeamMembersDTO teamMembersDTO);

    List<TeamMembers> teamMembersDTOsToTeamMembers(List<TeamMembersDTO> teamMembersDTOs);

    default SubTeam subTeamFromId(Long id) {
        if (id == null) {
            return null;
        }
        SubTeam subTeam = new SubTeam();
        subTeam.setId(id);
        return subTeam;
    }

    default Employee employeeFromId(Long id) {
        if (id == null) {
            return null;
        }
        Employee employee = new Employee();
        employee.setId(id);
        return employee;
    }
}
