package com.tcs.modulemanagement.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.tcs.modulemanagement.domain.TeamMembers;
import com.tcs.modulemanagement.service.TeamMembersService;
import com.tcs.modulemanagement.web.rest.util.HeaderUtil;
import com.tcs.modulemanagement.web.rest.dto.TeamMembersDTO;
import com.tcs.modulemanagement.web.rest.mapper.TeamMembersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing TeamMembers.
 */
@RestController
@RequestMapping("/api")
public class TeamMembersResource {

    private final Logger log = LoggerFactory.getLogger(TeamMembersResource.class);
        
    @Inject
    private TeamMembersService teamMembersService;
    
    @Inject
    private TeamMembersMapper teamMembersMapper;
    
    /**
     * POST  /team-members : Create a new teamMembers.
     *
     * @param teamMembersDTO the teamMembersDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new teamMembersDTO, or with status 400 (Bad Request) if the teamMembers has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/team-members",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TeamMembersDTO> createTeamMembers(@RequestBody TeamMembersDTO teamMembersDTO) throws URISyntaxException {
        log.debug("REST request to save TeamMembers : {}", teamMembersDTO);
        if (teamMembersDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("teamMembers", "idexists", "A new teamMembers cannot already have an ID")).body(null);
        }
        TeamMembersDTO result = teamMembersService.save(teamMembersDTO);
        return ResponseEntity.created(new URI("/api/team-members/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("teamMembers", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /team-members : Updates an existing teamMembers.
     *
     * @param teamMembersDTO the teamMembersDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated teamMembersDTO,
     * or with status 400 (Bad Request) if the teamMembersDTO is not valid,
     * or with status 500 (Internal Server Error) if the teamMembersDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/team-members",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TeamMembersDTO> updateTeamMembers(@RequestBody TeamMembersDTO teamMembersDTO) throws URISyntaxException {
        log.debug("REST request to update TeamMembers : {}", teamMembersDTO);
        if (teamMembersDTO.getId() == null) {
            return createTeamMembers(teamMembersDTO);
        }
        TeamMembersDTO result = teamMembersService.save(teamMembersDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("teamMembers", teamMembersDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /team-members : get all the teamMembers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of teamMembers in body
     */
    @RequestMapping(value = "/team-members",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TeamMembersDTO> getAllTeamMembers() {
        log.debug("REST request to get all TeamMembers");
        return teamMembersService.findAll();
    }

    /**
     * GET  /team-members/:id : get the "id" teamMembers.
     *
     * @param id the id of the teamMembersDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the teamMembersDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/team-members/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TeamMembersDTO> getTeamMembers(@PathVariable Long id) {
        log.debug("REST request to get TeamMembers : {}", id);
        TeamMembersDTO teamMembersDTO = teamMembersService.findOne(id);
        return Optional.ofNullable(teamMembersDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /team-members/:id : delete the "id" teamMembers.
     *
     * @param id the id of the teamMembersDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/team-members/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTeamMembers(@PathVariable Long id) {
        log.debug("REST request to delete TeamMembers : {}", id);
        teamMembersService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("teamMembers", id.toString())).build();
    }

}
