package com.tcs.modulemanagement.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.tcs.modulemanagement.domain.TeamLeads;
import com.tcs.modulemanagement.service.TeamLeadsService;
import com.tcs.modulemanagement.web.rest.util.HeaderUtil;
import com.tcs.modulemanagement.web.rest.dto.TeamLeadsDTO;
import com.tcs.modulemanagement.web.rest.mapper.TeamLeadsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing TeamLeads.
 */
@RestController
@RequestMapping("/api")
public class TeamLeadsResource {

    private final Logger log = LoggerFactory.getLogger(TeamLeadsResource.class);
        
    @Inject
    private TeamLeadsService teamLeadsService;
    
    @Inject
    private TeamLeadsMapper teamLeadsMapper;
    
    /**
     * POST  /team-leads : Create a new teamLeads.
     *
     * @param teamLeadsDTO the teamLeadsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new teamLeadsDTO, or with status 400 (Bad Request) if the teamLeads has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/team-leads",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TeamLeadsDTO> createTeamLeads(@RequestBody TeamLeadsDTO teamLeadsDTO) throws URISyntaxException {
        log.debug("REST request to save TeamLeads : {}", teamLeadsDTO);
        if (teamLeadsDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("teamLeads", "idexists", "A new teamLeads cannot already have an ID")).body(null);
        }
        TeamLeadsDTO result = teamLeadsService.save(teamLeadsDTO);
        return ResponseEntity.created(new URI("/api/team-leads/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("teamLeads", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /team-leads : Updates an existing teamLeads.
     *
     * @param teamLeadsDTO the teamLeadsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated teamLeadsDTO,
     * or with status 400 (Bad Request) if the teamLeadsDTO is not valid,
     * or with status 500 (Internal Server Error) if the teamLeadsDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/team-leads",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TeamLeadsDTO> updateTeamLeads(@RequestBody TeamLeadsDTO teamLeadsDTO) throws URISyntaxException {
        log.debug("REST request to update TeamLeads : {}", teamLeadsDTO);
        if (teamLeadsDTO.getId() == null) {
            return createTeamLeads(teamLeadsDTO);
        }
        TeamLeadsDTO result = teamLeadsService.save(teamLeadsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("teamLeads", teamLeadsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /team-leads : get all the teamLeads.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of teamLeads in body
     */
    @RequestMapping(value = "/team-leads",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TeamLeadsDTO> getAllTeamLeads() {
        log.debug("REST request to get all TeamLeads");
        return teamLeadsService.findAll();
    }

    /**
     * GET  /team-leads/:id : get the "id" teamLeads.
     *
     * @param id the id of the teamLeadsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the teamLeadsDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/team-leads/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TeamLeadsDTO> getTeamLeads(@PathVariable Long id) {
        log.debug("REST request to get TeamLeads : {}", id);
        TeamLeadsDTO teamLeadsDTO = teamLeadsService.findOne(id);
        return Optional.ofNullable(teamLeadsDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /team-leads/:id : delete the "id" teamLeads.
     *
     * @param id the id of the teamLeadsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/team-leads/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTeamLeads(@PathVariable Long id) {
        log.debug("REST request to delete TeamLeads : {}", id);
        teamLeadsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("teamLeads", id.toString())).build();
    }

}
