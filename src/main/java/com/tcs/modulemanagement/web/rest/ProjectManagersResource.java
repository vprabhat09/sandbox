package com.tcs.modulemanagement.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.tcs.modulemanagement.domain.ProjectManagers;
import com.tcs.modulemanagement.service.ProjectManagersService;
import com.tcs.modulemanagement.web.rest.util.HeaderUtil;
import com.tcs.modulemanagement.web.rest.dto.ProjectManagersDTO;
import com.tcs.modulemanagement.web.rest.mapper.ProjectManagersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing ProjectManagers.
 */
@RestController
@RequestMapping("/api")
public class ProjectManagersResource {

    private final Logger log = LoggerFactory.getLogger(ProjectManagersResource.class);
        
    @Inject
    private ProjectManagersService projectManagersService;
    
    @Inject
    private ProjectManagersMapper projectManagersMapper;
    
    /**
     * POST  /project-managers : Create a new projectManagers.
     *
     * @param projectManagersDTO the projectManagersDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new projectManagersDTO, or with status 400 (Bad Request) if the projectManagers has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/project-managers",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProjectManagersDTO> createProjectManagers(@RequestBody ProjectManagersDTO projectManagersDTO) throws URISyntaxException {
        log.debug("REST request to save ProjectManagers : {}", projectManagersDTO);
        if (projectManagersDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("projectManagers", "idexists", "A new projectManagers cannot already have an ID")).body(null);
        }
        ProjectManagersDTO result = projectManagersService.save(projectManagersDTO);
        return ResponseEntity.created(new URI("/api/project-managers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("projectManagers", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /project-managers : Updates an existing projectManagers.
     *
     * @param projectManagersDTO the projectManagersDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated projectManagersDTO,
     * or with status 400 (Bad Request) if the projectManagersDTO is not valid,
     * or with status 500 (Internal Server Error) if the projectManagersDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/project-managers",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProjectManagersDTO> updateProjectManagers(@RequestBody ProjectManagersDTO projectManagersDTO) throws URISyntaxException {
        log.debug("REST request to update ProjectManagers : {}", projectManagersDTO);
        if (projectManagersDTO.getId() == null) {
            return createProjectManagers(projectManagersDTO);
        }
        ProjectManagersDTO result = projectManagersService.save(projectManagersDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("projectManagers", projectManagersDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /project-managers : get all the projectManagers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of projectManagers in body
     */
    @RequestMapping(value = "/project-managers",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ProjectManagersDTO> getAllProjectManagers() {
        log.debug("REST request to get all ProjectManagers");
        return projectManagersService.findAll();
    }

    /**
     * GET  /project-managers/:id : get the "id" projectManagers.
     *
     * @param id the id of the projectManagersDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the projectManagersDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/project-managers/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProjectManagersDTO> getProjectManagers(@PathVariable Long id) {
        log.debug("REST request to get ProjectManagers : {}", id);
        ProjectManagersDTO projectManagersDTO = projectManagersService.findOne(id);
        return Optional.ofNullable(projectManagersDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /project-managers/:id : delete the "id" projectManagers.
     *
     * @param id the id of the projectManagersDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/project-managers/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteProjectManagers(@PathVariable Long id) {
        log.debug("REST request to delete ProjectManagers : {}", id);
        projectManagersService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("projectManagers", id.toString())).build();
    }

}
