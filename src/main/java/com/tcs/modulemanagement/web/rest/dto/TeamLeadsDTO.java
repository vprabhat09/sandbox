package com.tcs.modulemanagement.web.rest.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the TeamLeads entity.
 */
public class TeamLeadsDTO implements Serializable {

    private Long id;

    private Integer teamLeadId;


    private Long employeeId;
    
    private Long teamId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Integer getTeamLeadId() {
        return teamLeadId;
    }

    public void setTeamLeadId(Integer teamLeadId) {
        this.teamLeadId = teamLeadId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long subTeamId) {
        this.teamId = subTeamId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TeamLeadsDTO teamLeadsDTO = (TeamLeadsDTO) o;

        if ( ! Objects.equals(id, teamLeadsDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TeamLeadsDTO{" +
            "id=" + id +
            ", teamLeadId='" + teamLeadId + "'" +
            '}';
    }
}
