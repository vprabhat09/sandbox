package com.tcs.modulemanagement.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.tcs.modulemanagement.domain.Tcsaccount;
import com.tcs.modulemanagement.repository.TcsaccountRepository;
import com.tcs.modulemanagement.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Tcsaccount.
 */
@RestController
@RequestMapping("/api")
public class TcsaccountResource {

    private final Logger log = LoggerFactory.getLogger(TcsaccountResource.class);
        
    @Inject
    private TcsaccountRepository tcsaccountRepository;
    
    /**
     * POST  /tcsaccounts : Create a new tcsaccount.
     *
     * @param tcsaccount the tcsaccount to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tcsaccount, or with status 400 (Bad Request) if the tcsaccount has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/tcsaccounts",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tcsaccount> createTcsaccount(@RequestBody Tcsaccount tcsaccount) throws URISyntaxException {
        log.debug("REST request to save Tcsaccount : {}", tcsaccount);
        if (tcsaccount.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tcsaccount", "idexists", "A new tcsaccount cannot already have an ID")).body(null);
        }
        Tcsaccount result = tcsaccountRepository.save(tcsaccount);
        return ResponseEntity.created(new URI("/api/tcsaccounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("tcsaccount", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tcsaccounts : Updates an existing tcsaccount.
     *
     * @param tcsaccount the tcsaccount to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tcsaccount,
     * or with status 400 (Bad Request) if the tcsaccount is not valid,
     * or with status 500 (Internal Server Error) if the tcsaccount couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/tcsaccounts",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tcsaccount> updateTcsaccount(@RequestBody Tcsaccount tcsaccount) throws URISyntaxException {
        log.debug("REST request to update Tcsaccount : {}", tcsaccount);
        if (tcsaccount.getId() == null) {
            return createTcsaccount(tcsaccount);
        }
        Tcsaccount result = tcsaccountRepository.save(tcsaccount);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("tcsaccount", tcsaccount.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tcsaccounts : get all the tcsaccounts.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tcsaccounts in body
     */
    @RequestMapping(value = "/tcsaccounts",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Tcsaccount> getAllTcsaccounts() {
        log.debug("REST request to get all Tcsaccounts");
        List<Tcsaccount> tcsaccounts = tcsaccountRepository.findAll();
        return tcsaccounts;
    }

    /**
     * GET  /tcsaccounts/:id : get the "id" tcsaccount.
     *
     * @param id the id of the tcsaccount to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tcsaccount, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/tcsaccounts/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tcsaccount> getTcsaccount(@PathVariable Long id) {
        log.debug("REST request to get Tcsaccount : {}", id);
        Tcsaccount tcsaccount = tcsaccountRepository.findOne(id);
        return Optional.ofNullable(tcsaccount)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /tcsaccounts/:id : delete the "id" tcsaccount.
     *
     * @param id the id of the tcsaccount to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/tcsaccounts/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTcsaccount(@PathVariable Long id) {
        log.debug("REST request to delete Tcsaccount : {}", id);
        tcsaccountRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tcsaccount", id.toString())).build();
    }

}
