package com.tcs.modulemanagement.web.rest.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the SubTeam entity.
 */
public class SubTeamDTO implements Serializable {

    private Long id;

    private Integer subTeamId;

    private String subTeamName;


    private Long teamId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Integer getSubTeamId() {
        return subTeamId;
    }

    public void setSubTeamId(Integer subTeamId) {
        this.subTeamId = subTeamId;
    }
    public String getSubTeamName() {
        return subTeamName;
    }

    public void setSubTeamName(String subTeamName) {
        this.subTeamName = subTeamName;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SubTeamDTO subTeamDTO = (SubTeamDTO) o;

        if ( ! Objects.equals(id, subTeamDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SubTeamDTO{" +
            "id=" + id +
            ", subTeamId='" + subTeamId + "'" +
            ", subTeamName='" + subTeamName + "'" +
            '}';
    }
}
