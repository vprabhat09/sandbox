package com.tcs.modulemanagement.web.rest.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the AccountManagers entity.
 */
public class AccountManagersDTO implements Serializable {

    private Long id;

    private Integer managerId;


    private Long tcsAccountId;
    
    private Long employeeId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Integer getManagerId() {
        return managerId;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }

    public Long getTcsAccountId() {
        return tcsAccountId;
    }

    public void setTcsAccountId(Long tcsaccountId) {
        this.tcsAccountId = tcsaccountId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AccountManagersDTO accountManagersDTO = (AccountManagersDTO) o;

        if ( ! Objects.equals(id, accountManagersDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "AccountManagersDTO{" +
            "id=" + id +
            ", managerId='" + managerId + "'" +
            '}';
    }
}
