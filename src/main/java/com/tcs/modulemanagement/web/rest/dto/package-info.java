/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.tcs.modulemanagement.web.rest.dto;
