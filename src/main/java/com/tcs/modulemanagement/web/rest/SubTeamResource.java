package com.tcs.modulemanagement.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.tcs.modulemanagement.domain.SubTeam;
import com.tcs.modulemanagement.service.SubTeamService;
import com.tcs.modulemanagement.web.rest.util.HeaderUtil;
import com.tcs.modulemanagement.web.rest.dto.SubTeamDTO;
import com.tcs.modulemanagement.web.rest.mapper.SubTeamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing SubTeam.
 */
@RestController
@RequestMapping("/api")
public class SubTeamResource {

    private final Logger log = LoggerFactory.getLogger(SubTeamResource.class);
        
    @Inject
    private SubTeamService subTeamService;
    
    @Inject
    private SubTeamMapper subTeamMapper;
    
    /**
     * POST  /sub-teams : Create a new subTeam.
     *
     * @param subTeamDTO the subTeamDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new subTeamDTO, or with status 400 (Bad Request) if the subTeam has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/sub-teams",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SubTeamDTO> createSubTeam(@RequestBody SubTeamDTO subTeamDTO) throws URISyntaxException {
        log.debug("REST request to save SubTeam : {}", subTeamDTO);
        if (subTeamDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("subTeam", "idexists", "A new subTeam cannot already have an ID")).body(null);
        }
        SubTeamDTO result = subTeamService.save(subTeamDTO);
        return ResponseEntity.created(new URI("/api/sub-teams/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("subTeam", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sub-teams : Updates an existing subTeam.
     *
     * @param subTeamDTO the subTeamDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated subTeamDTO,
     * or with status 400 (Bad Request) if the subTeamDTO is not valid,
     * or with status 500 (Internal Server Error) if the subTeamDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/sub-teams",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SubTeamDTO> updateSubTeam(@RequestBody SubTeamDTO subTeamDTO) throws URISyntaxException {
        log.debug("REST request to update SubTeam : {}", subTeamDTO);
        if (subTeamDTO.getId() == null) {
            return createSubTeam(subTeamDTO);
        }
        SubTeamDTO result = subTeamService.save(subTeamDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("subTeam", subTeamDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sub-teams : get all the subTeams.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of subTeams in body
     */
    @RequestMapping(value = "/sub-teams",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SubTeamDTO> getAllSubTeams() {
        log.debug("REST request to get all SubTeams");
        return subTeamService.findAll();
    }

    /**
     * GET  /sub-teams/:id : get the "id" subTeam.
     *
     * @param id the id of the subTeamDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the subTeamDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/sub-teams/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SubTeamDTO> getSubTeam(@PathVariable Long id) {
        log.debug("REST request to get SubTeam : {}", id);
        SubTeamDTO subTeamDTO = subTeamService.findOne(id);
        return Optional.ofNullable(subTeamDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /sub-teams/:id : delete the "id" subTeam.
     *
     * @param id the id of the subTeamDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/sub-teams/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSubTeam(@PathVariable Long id) {
        log.debug("REST request to delete SubTeam : {}", id);
        subTeamService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("subTeam", id.toString())).build();
    }

}
