package com.tcs.modulemanagement.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.tcs.modulemanagement.domain.TeamManagers;
import com.tcs.modulemanagement.service.TeamManagersService;
import com.tcs.modulemanagement.web.rest.util.HeaderUtil;
import com.tcs.modulemanagement.web.rest.dto.TeamManagersDTO;
import com.tcs.modulemanagement.web.rest.mapper.TeamManagersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing TeamManagers.
 */
@RestController
@RequestMapping("/api")
public class TeamManagersResource {

    private final Logger log = LoggerFactory.getLogger(TeamManagersResource.class);
        
    @Inject
    private TeamManagersService teamManagersService;
    
    @Inject
    private TeamManagersMapper teamManagersMapper;
    
    /**
     * POST  /team-managers : Create a new teamManagers.
     *
     * @param teamManagersDTO the teamManagersDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new teamManagersDTO, or with status 400 (Bad Request) if the teamManagers has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/team-managers",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TeamManagersDTO> createTeamManagers(@RequestBody TeamManagersDTO teamManagersDTO) throws URISyntaxException {
        log.debug("REST request to save TeamManagers : {}", teamManagersDTO);
        if (teamManagersDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("teamManagers", "idexists", "A new teamManagers cannot already have an ID")).body(null);
        }
        TeamManagersDTO result = teamManagersService.save(teamManagersDTO);
        return ResponseEntity.created(new URI("/api/team-managers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("teamManagers", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /team-managers : Updates an existing teamManagers.
     *
     * @param teamManagersDTO the teamManagersDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated teamManagersDTO,
     * or with status 400 (Bad Request) if the teamManagersDTO is not valid,
     * or with status 500 (Internal Server Error) if the teamManagersDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/team-managers",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TeamManagersDTO> updateTeamManagers(@RequestBody TeamManagersDTO teamManagersDTO) throws URISyntaxException {
        log.debug("REST request to update TeamManagers : {}", teamManagersDTO);
        if (teamManagersDTO.getId() == null) {
            return createTeamManagers(teamManagersDTO);
        }
        TeamManagersDTO result = teamManagersService.save(teamManagersDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("teamManagers", teamManagersDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /team-managers : get all the teamManagers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of teamManagers in body
     */
    @RequestMapping(value = "/team-managers",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TeamManagersDTO> getAllTeamManagers() {
        log.debug("REST request to get all TeamManagers");
        return teamManagersService.findAll();
    }

    /**
     * GET  /team-managers/:id : get the "id" teamManagers.
     *
     * @param id the id of the teamManagersDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the teamManagersDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/team-managers/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TeamManagersDTO> getTeamManagers(@PathVariable Long id) {
        log.debug("REST request to get TeamManagers : {}", id);
        TeamManagersDTO teamManagersDTO = teamManagersService.findOne(id);
        return Optional.ofNullable(teamManagersDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /team-managers/:id : delete the "id" teamManagers.
     *
     * @param id the id of the teamManagersDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/team-managers/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTeamManagers(@PathVariable Long id) {
        log.debug("REST request to delete TeamManagers : {}", id);
        teamManagersService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("teamManagers", id.toString())).build();
    }

}
