package com.tcs.modulemanagement.web.rest.mapper;

import com.tcs.modulemanagement.domain.*;
import com.tcs.modulemanagement.web.rest.dto.TeamManagersDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity TeamManagers and its DTO TeamManagersDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TeamManagersMapper {

    @Mapping(source = "employee.id", target = "employeeId")
    @Mapping(source = "team.id", target = "teamId")
    TeamManagersDTO teamManagersToTeamManagersDTO(TeamManagers teamManagers);

    List<TeamManagersDTO> teamManagersToTeamManagersDTOs(List<TeamManagers> teamManagers);

    @Mapping(source = "employeeId", target = "employee")
    @Mapping(source = "teamId", target = "team")
    TeamManagers teamManagersDTOToTeamManagers(TeamManagersDTO teamManagersDTO);

    List<TeamManagers> teamManagersDTOsToTeamManagers(List<TeamManagersDTO> teamManagersDTOs);

    default Employee employeeFromId(Long id) {
        if (id == null) {
            return null;
        }
        Employee employee = new Employee();
        employee.setId(id);
        return employee;
    }

    default Team teamFromId(Long id) {
        if (id == null) {
            return null;
        }
        Team team = new Team();
        team.setId(id);
        return team;
    }
}
