package com.tcs.modulemanagement.web.rest.mapper;

import com.tcs.modulemanagement.domain.*;
import com.tcs.modulemanagement.web.rest.dto.AccountManagersDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity AccountManagers and its DTO AccountManagersDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AccountManagersMapper {

    @Mapping(source = "tcsAccount.id", target = "tcsAccountId")
    @Mapping(source = "employee.id", target = "employeeId")
    AccountManagersDTO accountManagersToAccountManagersDTO(AccountManagers accountManagers);

    List<AccountManagersDTO> accountManagersToAccountManagersDTOs(List<AccountManagers> accountManagers);

    @Mapping(source = "tcsAccountId", target = "tcsAccount")
    @Mapping(source = "employeeId", target = "employee")
    AccountManagers accountManagersDTOToAccountManagers(AccountManagersDTO accountManagersDTO);

    List<AccountManagers> accountManagersDTOsToAccountManagers(List<AccountManagersDTO> accountManagersDTOs);

    default Tcsaccount tcsaccountFromId(Long id) {
        if (id == null) {
            return null;
        }
        Tcsaccount tcsaccount = new Tcsaccount();
        tcsaccount.setId(id);
        return tcsaccount;
    }

    default Employee employeeFromId(Long id) {
        if (id == null) {
            return null;
        }
        Employee employee = new Employee();
        employee.setId(id);
        return employee;
    }
}
