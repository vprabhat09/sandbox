package com.tcs.modulemanagement.service;

import com.tcs.modulemanagement.domain.Employee;
import com.tcs.modulemanagement.web.rest.dto.EmployeeDTO;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Employee.
 */
public interface EmployeeService {

    /**
     * Save a employee.
     *
     * @param employeeDTO the entity to save
     * @return the persisted entity
     */
    EmployeeDTO save(EmployeeDTO employeeDTO);

    /**
     *  Get all the employees.
     *
     *  @return the list of entities
     */
    List<EmployeeDTO> findAll();
    /**
     *  Get all the employees where AccountManager is null.
     *
     *  @return the list of entities
     */
    List<EmployeeDTO> findAllWhereAccountManagerIsNull();
    /**
     *  Get all the employees where TeamManger is null.
     *
     *  @return the list of entities
     */
    List<EmployeeDTO> findAllWhereTeamMangerIsNull();
    /**
     *  Get all the employees where ProjectManager is null.
     *
     *  @return the list of entities
     */
    List<EmployeeDTO> findAllWhereProjectManagerIsNull();
    /**
     *  Get all the employees where TeamLead is null.
     *
     *  @return the list of entities
     */
    List<EmployeeDTO> findAllWhereTeamLeadIsNull();
    /**
     *  Get all the employees where TeamMembers is null.
     *
     *  @return the list of entities
     */
    List<EmployeeDTO> findAllWhereTeamMembersIsNull();
    /**
     *  Get all the employees where Seat is null.
     *
     *  @return the list of entities
     */
    List<EmployeeDTO> findAllWhereSeatIsNull();

    /**
     *  Get the "id" employee.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    EmployeeDTO findOne(Long id);

    EmployeeDTO findByEmpId(int id);

    /**
     *  Delete the "id" employee.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
