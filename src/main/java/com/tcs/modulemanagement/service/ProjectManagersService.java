package com.tcs.modulemanagement.service;

import com.tcs.modulemanagement.domain.ProjectManagers;
import com.tcs.modulemanagement.web.rest.dto.ProjectManagersDTO;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing ProjectManagers.
 */
public interface ProjectManagersService {

    /**
     * Save a projectManagers.
     * 
     * @param projectManagersDTO the entity to save
     * @return the persisted entity
     */
    ProjectManagersDTO save(ProjectManagersDTO projectManagersDTO);

    /**
     *  Get all the projectManagers.
     *  
     *  @return the list of entities
     */
    List<ProjectManagersDTO> findAll();

    /**
     *  Get the "id" projectManagers.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    ProjectManagersDTO findOne(Long id);

    /**
     *  Delete the "id" projectManagers.
     *  
     *  @param id the id of the entity
     */
    void delete(Long id);
}
