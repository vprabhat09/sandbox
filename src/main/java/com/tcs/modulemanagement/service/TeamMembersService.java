package com.tcs.modulemanagement.service;

import com.tcs.modulemanagement.domain.TeamMembers;
import com.tcs.modulemanagement.web.rest.dto.TeamMembersDTO;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing TeamMembers.
 */
public interface TeamMembersService {

    /**
     * Save a teamMembers.
     * 
     * @param teamMembersDTO the entity to save
     * @return the persisted entity
     */
    TeamMembersDTO save(TeamMembersDTO teamMembersDTO);

    /**
     *  Get all the teamMembers.
     *  
     *  @return the list of entities
     */
    List<TeamMembersDTO> findAll();

    /**
     *  Get the "id" teamMembers.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    TeamMembersDTO findOne(Long id);

    /**
     *  Delete the "id" teamMembers.
     *  
     *  @param id the id of the entity
     */
    void delete(Long id);
}
