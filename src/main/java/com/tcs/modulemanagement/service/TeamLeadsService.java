package com.tcs.modulemanagement.service;

import com.tcs.modulemanagement.domain.TeamLeads;
import com.tcs.modulemanagement.web.rest.dto.TeamLeadsDTO;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing TeamLeads.
 */
public interface TeamLeadsService {

    /**
     * Save a teamLeads.
     * 
     * @param teamLeadsDTO the entity to save
     * @return the persisted entity
     */
    TeamLeadsDTO save(TeamLeadsDTO teamLeadsDTO);

    /**
     *  Get all the teamLeads.
     *  
     *  @return the list of entities
     */
    List<TeamLeadsDTO> findAll();

    /**
     *  Get the "id" teamLeads.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    TeamLeadsDTO findOne(Long id);

    /**
     *  Delete the "id" teamLeads.
     *  
     *  @param id the id of the entity
     */
    void delete(Long id);
}
