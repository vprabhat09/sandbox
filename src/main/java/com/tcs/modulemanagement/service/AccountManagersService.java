package com.tcs.modulemanagement.service;

import com.tcs.modulemanagement.domain.AccountManagers;
import com.tcs.modulemanagement.web.rest.dto.AccountManagersDTO;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing AccountManagers.
 */
public interface AccountManagersService {

    /**
     * Save a accountManagers.
     * 
     * @param accountManagersDTO the entity to save
     * @return the persisted entity
     */
    AccountManagersDTO save(AccountManagersDTO accountManagersDTO);

    /**
     *  Get all the accountManagers.
     *  
     *  @return the list of entities
     */
    List<AccountManagersDTO> findAll();

    /**
     *  Get the "id" accountManagers.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    AccountManagersDTO findOne(Long id);

    /**
     *  Delete the "id" accountManagers.
     *  
     *  @param id the id of the entity
     */
    void delete(Long id);
}
