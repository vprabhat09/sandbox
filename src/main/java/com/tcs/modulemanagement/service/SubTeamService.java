package com.tcs.modulemanagement.service;

import com.tcs.modulemanagement.domain.SubTeam;
import com.tcs.modulemanagement.web.rest.dto.SubTeamDTO;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing SubTeam.
 */
public interface SubTeamService {

    /**
     * Save a subTeam.
     * 
     * @param subTeamDTO the entity to save
     * @return the persisted entity
     */
    SubTeamDTO save(SubTeamDTO subTeamDTO);

    /**
     *  Get all the subTeams.
     *  
     *  @return the list of entities
     */
    List<SubTeamDTO> findAll();

    /**
     *  Get the "id" subTeam.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    SubTeamDTO findOne(Long id);

    /**
     *  Delete the "id" subTeam.
     *  
     *  @param id the id of the entity
     */
    void delete(Long id);
}
