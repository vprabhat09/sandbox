package com.tcs.modulemanagement.service;

import com.tcs.modulemanagement.domain.TeamManagers;
import com.tcs.modulemanagement.web.rest.dto.TeamManagersDTO;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing TeamManagers.
 */
public interface TeamManagersService {

    /**
     * Save a teamManagers.
     * 
     * @param teamManagersDTO the entity to save
     * @return the persisted entity
     */
    TeamManagersDTO save(TeamManagersDTO teamManagersDTO);

    /**
     *  Get all the teamManagers.
     *  
     *  @return the list of entities
     */
    List<TeamManagersDTO> findAll();

    /**
     *  Get the "id" teamManagers.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    TeamManagersDTO findOne(Long id);

    /**
     *  Delete the "id" teamManagers.
     *  
     *  @param id the id of the entity
     */
    void delete(Long id);
}
