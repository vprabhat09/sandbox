package com.tcs.modulemanagement.service.impl;

import com.tcs.modulemanagement.service.EmployeeService;
import com.tcs.modulemanagement.domain.Employee;
import com.tcs.modulemanagement.repository.EmployeeRepository;
import com.tcs.modulemanagement.web.rest.dto.EmployeeDTO;
import com.tcs.modulemanagement.web.rest.mapper.EmployeeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing Employee.
 */
@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService{

    private final Logger log = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Inject
    private EmployeeRepository employeeRepository;

    @Inject
    private EmployeeMapper employeeMapper;

    /**
     * Save a employee.
     *
     * @param employeeDTO the entity to save
     * @return the persisted entity
     */
    public EmployeeDTO save(EmployeeDTO employeeDTO) {
        log.debug("Request to save Employee : {}", employeeDTO);
        Employee employee = employeeMapper.employeeDTOToEmployee(employeeDTO);
        employee = employeeRepository.save(employee);
        EmployeeDTO result = employeeMapper.employeeToEmployeeDTO(employee);
        return result;
    }

    /**
     *  Get all the employees.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<EmployeeDTO> findAll() {
        log.debug("Request to get all Employees");
        List<EmployeeDTO> result = employeeRepository.findAll().stream()
            .map(employeeMapper::employeeToEmployeeDTO)
            .collect(Collectors.toCollection(LinkedList::new));
        return result;
    }


    /**
     *  get all the employees where AccountManager is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<EmployeeDTO> findAllWhereAccountManagerIsNull() {
        log.debug("Request to get all employees where AccountManager is null");
        return StreamSupport
            .stream(employeeRepository.findAll().spliterator(), false)
            .filter(employee -> employee.getAccountManager() == null)
            .map(employeeMapper::employeeToEmployeeDTO)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     *  get all the employees where TeamManger is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<EmployeeDTO> findAllWhereTeamMangerIsNull() {
        log.debug("Request to get all employees where TeamManger is null");
        return StreamSupport
            .stream(employeeRepository.findAll().spliterator(), false)
            .filter(employee -> employee.getTeamManger() == null)
            .map(employeeMapper::employeeToEmployeeDTO)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     *  get all the employees where ProjectManager is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<EmployeeDTO> findAllWhereProjectManagerIsNull() {
        log.debug("Request to get all employees where ProjectManager is null");
        return StreamSupport
            .stream(employeeRepository.findAll().spliterator(), false)
            .filter(employee -> employee.getProjectManager() == null)
            .map(employeeMapper::employeeToEmployeeDTO)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     *  get all the employees where TeamLead is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<EmployeeDTO> findAllWhereTeamLeadIsNull() {
        log.debug("Request to get all employees where TeamLead is null");
        return StreamSupport
            .stream(employeeRepository.findAll().spliterator(), false)
            .filter(employee -> employee.getTeamLead() == null)
            .map(employeeMapper::employeeToEmployeeDTO)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     *  get all the employees where TeamMembers is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<EmployeeDTO> findAllWhereTeamMembersIsNull() {
        log.debug("Request to get all employees where TeamMembers is null");
        return StreamSupport
            .stream(employeeRepository.findAll().spliterator(), false)
            .filter(employee -> employee.getTeamMembers() == null)
            .map(employeeMapper::employeeToEmployeeDTO)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     *  get all the employees where Seat is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<EmployeeDTO> findAllWhereSeatIsNull() {
        log.debug("Request to get all employees where Seat is null");
        return StreamSupport
            .stream(employeeRepository.findAll().spliterator(), false)
            .filter(employee -> employee.getSeat() == null)
            .map(employeeMapper::employeeToEmployeeDTO)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one employee by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public EmployeeDTO findOne(Long id) {
        log.debug("Request to get Employee : {}", id);
        Employee employee = employeeRepository.findOne(id);
        EmployeeDTO employeeDTO = employeeMapper.employeeToEmployeeDTO(employee);
        return employeeDTO;
    }

    @Transactional(readOnly = true)
    public EmployeeDTO findByEmpId(int id) {
        log.debug("Request to get Employee : {}", id);
        Employee employee = employeeRepository.findByEmpId(id);
        EmployeeDTO employeeDTO = employeeMapper.employeeToEmployeeDTO(employee);
        return employeeDTO;
    }

    /**
     *  Delete the  employee by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Employee : {}", id);
        employeeRepository.delete(id);
    }
}
