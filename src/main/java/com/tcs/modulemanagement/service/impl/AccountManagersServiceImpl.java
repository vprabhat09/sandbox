package com.tcs.modulemanagement.service.impl;

import com.tcs.modulemanagement.service.AccountManagersService;
import com.tcs.modulemanagement.domain.AccountManagers;
import com.tcs.modulemanagement.repository.AccountManagersRepository;
import com.tcs.modulemanagement.web.rest.dto.AccountManagersDTO;
import com.tcs.modulemanagement.web.rest.mapper.AccountManagersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing AccountManagers.
 */
@Service
@Transactional
public class AccountManagersServiceImpl implements AccountManagersService{

    private final Logger log = LoggerFactory.getLogger(AccountManagersServiceImpl.class);
    
    @Inject
    private AccountManagersRepository accountManagersRepository;
    
    @Inject
    private AccountManagersMapper accountManagersMapper;
    
    /**
     * Save a accountManagers.
     * 
     * @param accountManagersDTO the entity to save
     * @return the persisted entity
     */
    public AccountManagersDTO save(AccountManagersDTO accountManagersDTO) {
        log.debug("Request to save AccountManagers : {}", accountManagersDTO);
        AccountManagers accountManagers = accountManagersMapper.accountManagersDTOToAccountManagers(accountManagersDTO);
        accountManagers = accountManagersRepository.save(accountManagers);
        AccountManagersDTO result = accountManagersMapper.accountManagersToAccountManagersDTO(accountManagers);
        return result;
    }

    /**
     *  Get all the accountManagers.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<AccountManagersDTO> findAll() {
        log.debug("Request to get all AccountManagers");
        List<AccountManagersDTO> result = accountManagersRepository.findAll().stream()
            .map(accountManagersMapper::accountManagersToAccountManagersDTO)
            .collect(Collectors.toCollection(LinkedList::new));
        return result;
    }

    /**
     *  Get one accountManagers by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public AccountManagersDTO findOne(Long id) {
        log.debug("Request to get AccountManagers : {}", id);
        AccountManagers accountManagers = accountManagersRepository.findOne(id);
        AccountManagersDTO accountManagersDTO = accountManagersMapper.accountManagersToAccountManagersDTO(accountManagers);
        return accountManagersDTO;
    }

    /**
     *  Delete the  accountManagers by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AccountManagers : {}", id);
        accountManagersRepository.delete(id);
    }
}
