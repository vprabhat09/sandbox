package com.tcs.modulemanagement.service.impl;

import com.tcs.modulemanagement.service.TeamManagersService;
import com.tcs.modulemanagement.domain.TeamManagers;
import com.tcs.modulemanagement.repository.TeamManagersRepository;
import com.tcs.modulemanagement.web.rest.dto.TeamManagersDTO;
import com.tcs.modulemanagement.web.rest.mapper.TeamManagersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing TeamManagers.
 */
@Service
@Transactional
public class TeamManagersServiceImpl implements TeamManagersService{

    private final Logger log = LoggerFactory.getLogger(TeamManagersServiceImpl.class);
    
    @Inject
    private TeamManagersRepository teamManagersRepository;
    
    @Inject
    private TeamManagersMapper teamManagersMapper;
    
    /**
     * Save a teamManagers.
     * 
     * @param teamManagersDTO the entity to save
     * @return the persisted entity
     */
    public TeamManagersDTO save(TeamManagersDTO teamManagersDTO) {
        log.debug("Request to save TeamManagers : {}", teamManagersDTO);
        TeamManagers teamManagers = teamManagersMapper.teamManagersDTOToTeamManagers(teamManagersDTO);
        teamManagers = teamManagersRepository.save(teamManagers);
        TeamManagersDTO result = teamManagersMapper.teamManagersToTeamManagersDTO(teamManagers);
        return result;
    }

    /**
     *  Get all the teamManagers.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<TeamManagersDTO> findAll() {
        log.debug("Request to get all TeamManagers");
        List<TeamManagersDTO> result = teamManagersRepository.findAll().stream()
            .map(teamManagersMapper::teamManagersToTeamManagersDTO)
            .collect(Collectors.toCollection(LinkedList::new));
        return result;
    }

    /**
     *  Get one teamManagers by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public TeamManagersDTO findOne(Long id) {
        log.debug("Request to get TeamManagers : {}", id);
        TeamManagers teamManagers = teamManagersRepository.findOne(id);
        TeamManagersDTO teamManagersDTO = teamManagersMapper.teamManagersToTeamManagersDTO(teamManagers);
        return teamManagersDTO;
    }

    /**
     *  Delete the  teamManagers by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TeamManagers : {}", id);
        teamManagersRepository.delete(id);
    }
}
