package com.tcs.modulemanagement.service.impl;

import com.tcs.modulemanagement.service.TeamMembersService;
import com.tcs.modulemanagement.domain.TeamMembers;
import com.tcs.modulemanagement.repository.TeamMembersRepository;
import com.tcs.modulemanagement.web.rest.dto.TeamMembersDTO;
import com.tcs.modulemanagement.web.rest.mapper.TeamMembersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing TeamMembers.
 */
@Service
@Transactional
public class TeamMembersServiceImpl implements TeamMembersService{

    private final Logger log = LoggerFactory.getLogger(TeamMembersServiceImpl.class);
    
    @Inject
    private TeamMembersRepository teamMembersRepository;
    
    @Inject
    private TeamMembersMapper teamMembersMapper;
    
    /**
     * Save a teamMembers.
     * 
     * @param teamMembersDTO the entity to save
     * @return the persisted entity
     */
    public TeamMembersDTO save(TeamMembersDTO teamMembersDTO) {
        log.debug("Request to save TeamMembers : {}", teamMembersDTO);
        TeamMembers teamMembers = teamMembersMapper.teamMembersDTOToTeamMembers(teamMembersDTO);
        teamMembers = teamMembersRepository.save(teamMembers);
        TeamMembersDTO result = teamMembersMapper.teamMembersToTeamMembersDTO(teamMembers);
        return result;
    }

    /**
     *  Get all the teamMembers.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<TeamMembersDTO> findAll() {
        log.debug("Request to get all TeamMembers");
        List<TeamMembersDTO> result = teamMembersRepository.findAll().stream()
            .map(teamMembersMapper::teamMembersToTeamMembersDTO)
            .collect(Collectors.toCollection(LinkedList::new));
        return result;
    }

    /**
     *  Get one teamMembers by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public TeamMembersDTO findOne(Long id) {
        log.debug("Request to get TeamMembers : {}", id);
        TeamMembers teamMembers = teamMembersRepository.findOne(id);
        TeamMembersDTO teamMembersDTO = teamMembersMapper.teamMembersToTeamMembersDTO(teamMembers);
        return teamMembersDTO;
    }

    /**
     *  Delete the  teamMembers by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TeamMembers : {}", id);
        teamMembersRepository.delete(id);
    }
}
