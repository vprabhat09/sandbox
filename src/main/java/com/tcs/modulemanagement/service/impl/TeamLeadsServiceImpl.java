package com.tcs.modulemanagement.service.impl;

import com.tcs.modulemanagement.service.TeamLeadsService;
import com.tcs.modulemanagement.domain.TeamLeads;
import com.tcs.modulemanagement.repository.TeamLeadsRepository;
import com.tcs.modulemanagement.web.rest.dto.TeamLeadsDTO;
import com.tcs.modulemanagement.web.rest.mapper.TeamLeadsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing TeamLeads.
 */
@Service
@Transactional
public class TeamLeadsServiceImpl implements TeamLeadsService{

    private final Logger log = LoggerFactory.getLogger(TeamLeadsServiceImpl.class);
    
    @Inject
    private TeamLeadsRepository teamLeadsRepository;
    
    @Inject
    private TeamLeadsMapper teamLeadsMapper;
    
    /**
     * Save a teamLeads.
     * 
     * @param teamLeadsDTO the entity to save
     * @return the persisted entity
     */
    public TeamLeadsDTO save(TeamLeadsDTO teamLeadsDTO) {
        log.debug("Request to save TeamLeads : {}", teamLeadsDTO);
        TeamLeads teamLeads = teamLeadsMapper.teamLeadsDTOToTeamLeads(teamLeadsDTO);
        teamLeads = teamLeadsRepository.save(teamLeads);
        TeamLeadsDTO result = teamLeadsMapper.teamLeadsToTeamLeadsDTO(teamLeads);
        return result;
    }

    /**
     *  Get all the teamLeads.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<TeamLeadsDTO> findAll() {
        log.debug("Request to get all TeamLeads");
        List<TeamLeadsDTO> result = teamLeadsRepository.findAll().stream()
            .map(teamLeadsMapper::teamLeadsToTeamLeadsDTO)
            .collect(Collectors.toCollection(LinkedList::new));
        return result;
    }

    /**
     *  Get one teamLeads by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public TeamLeadsDTO findOne(Long id) {
        log.debug("Request to get TeamLeads : {}", id);
        TeamLeads teamLeads = teamLeadsRepository.findOne(id);
        TeamLeadsDTO teamLeadsDTO = teamLeadsMapper.teamLeadsToTeamLeadsDTO(teamLeads);
        return teamLeadsDTO;
    }

    /**
     *  Delete the  teamLeads by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TeamLeads : {}", id);
        teamLeadsRepository.delete(id);
    }
}
