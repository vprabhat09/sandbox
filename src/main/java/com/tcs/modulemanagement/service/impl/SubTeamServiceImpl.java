package com.tcs.modulemanagement.service.impl;

import com.tcs.modulemanagement.service.SubTeamService;
import com.tcs.modulemanagement.domain.SubTeam;
import com.tcs.modulemanagement.repository.SubTeamRepository;
import com.tcs.modulemanagement.web.rest.dto.SubTeamDTO;
import com.tcs.modulemanagement.web.rest.mapper.SubTeamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing SubTeam.
 */
@Service
@Transactional
public class SubTeamServiceImpl implements SubTeamService{

    private final Logger log = LoggerFactory.getLogger(SubTeamServiceImpl.class);
    
    @Inject
    private SubTeamRepository subTeamRepository;
    
    @Inject
    private SubTeamMapper subTeamMapper;
    
    /**
     * Save a subTeam.
     * 
     * @param subTeamDTO the entity to save
     * @return the persisted entity
     */
    public SubTeamDTO save(SubTeamDTO subTeamDTO) {
        log.debug("Request to save SubTeam : {}", subTeamDTO);
        SubTeam subTeam = subTeamMapper.subTeamDTOToSubTeam(subTeamDTO);
        subTeam = subTeamRepository.save(subTeam);
        SubTeamDTO result = subTeamMapper.subTeamToSubTeamDTO(subTeam);
        return result;
    }

    /**
     *  Get all the subTeams.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<SubTeamDTO> findAll() {
        log.debug("Request to get all SubTeams");
        List<SubTeamDTO> result = subTeamRepository.findAll().stream()
            .map(subTeamMapper::subTeamToSubTeamDTO)
            .collect(Collectors.toCollection(LinkedList::new));
        return result;
    }

    /**
     *  Get one subTeam by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public SubTeamDTO findOne(Long id) {
        log.debug("Request to get SubTeam : {}", id);
        SubTeam subTeam = subTeamRepository.findOne(id);
        SubTeamDTO subTeamDTO = subTeamMapper.subTeamToSubTeamDTO(subTeam);
        return subTeamDTO;
    }

    /**
     *  Delete the  subTeam by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SubTeam : {}", id);
        subTeamRepository.delete(id);
    }
}
