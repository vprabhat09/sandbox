package com.tcs.modulemanagement.service.impl;

import com.tcs.modulemanagement.service.ProjectManagersService;
import com.tcs.modulemanagement.domain.ProjectManagers;
import com.tcs.modulemanagement.repository.ProjectManagersRepository;
import com.tcs.modulemanagement.web.rest.dto.ProjectManagersDTO;
import com.tcs.modulemanagement.web.rest.mapper.ProjectManagersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ProjectManagers.
 */
@Service
@Transactional
public class ProjectManagersServiceImpl implements ProjectManagersService{

    private final Logger log = LoggerFactory.getLogger(ProjectManagersServiceImpl.class);
    
    @Inject
    private ProjectManagersRepository projectManagersRepository;
    
    @Inject
    private ProjectManagersMapper projectManagersMapper;
    
    /**
     * Save a projectManagers.
     * 
     * @param projectManagersDTO the entity to save
     * @return the persisted entity
     */
    public ProjectManagersDTO save(ProjectManagersDTO projectManagersDTO) {
        log.debug("Request to save ProjectManagers : {}", projectManagersDTO);
        ProjectManagers projectManagers = projectManagersMapper.projectManagersDTOToProjectManagers(projectManagersDTO);
        projectManagers = projectManagersRepository.save(projectManagers);
        ProjectManagersDTO result = projectManagersMapper.projectManagersToProjectManagersDTO(projectManagers);
        return result;
    }

    /**
     *  Get all the projectManagers.
     *  
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<ProjectManagersDTO> findAll() {
        log.debug("Request to get all ProjectManagers");
        List<ProjectManagersDTO> result = projectManagersRepository.findAll().stream()
            .map(projectManagersMapper::projectManagersToProjectManagersDTO)
            .collect(Collectors.toCollection(LinkedList::new));
        return result;
    }

    /**
     *  Get one projectManagers by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public ProjectManagersDTO findOne(Long id) {
        log.debug("Request to get ProjectManagers : {}", id);
        ProjectManagers projectManagers = projectManagersRepository.findOne(id);
        ProjectManagersDTO projectManagersDTO = projectManagersMapper.projectManagersToProjectManagersDTO(projectManagers);
        return projectManagersDTO;
    }

    /**
     *  Delete the  projectManagers by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ProjectManagers : {}", id);
        projectManagersRepository.delete(id);
    }
}
