package com.tcs.modulemanagement.web.rest;

import com.tcs.modulemanagement.ModulemanagementV3App;
import com.tcs.modulemanagement.domain.Seat;
import com.tcs.modulemanagement.repository.SeatRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SeatResource REST controller.
 *
 * @see SeatResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ModulemanagementV3App.class)
@WebAppConfiguration
@IntegrationTest
public class SeatResourceIntTest {


    private static final Integer DEFAULT_SEAT_ID = 1;
    private static final Integer UPDATED_SEAT_ID = 2;
    private static final String DEFAULT_SEAT_NAME = "AAAAA";
    private static final String UPDATED_SEAT_NAME = "BBBBB";

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    @Inject
    private SeatRepository seatRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSeatMockMvc;

    private Seat seat;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SeatResource seatResource = new SeatResource();
        ReflectionTestUtils.setField(seatResource, "seatRepository", seatRepository);
        this.restSeatMockMvc = MockMvcBuilders.standaloneSetup(seatResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        seat = new Seat();
        seat.setSeatId(DEFAULT_SEAT_ID);
        seat.setSeatName(DEFAULT_SEAT_NAME);
        seat.setStatus(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createSeat() throws Exception {
        int databaseSizeBeforeCreate = seatRepository.findAll().size();

        // Create the Seat

        restSeatMockMvc.perform(post("/api/seats")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(seat)))
                .andExpect(status().isCreated());

        // Validate the Seat in the database
        List<Seat> seats = seatRepository.findAll();
        assertThat(seats).hasSize(databaseSizeBeforeCreate + 1);
        Seat testSeat = seats.get(seats.size() - 1);
        assertThat(testSeat.getSeatId()).isEqualTo(DEFAULT_SEAT_ID);
        assertThat(testSeat.getSeatName()).isEqualTo(DEFAULT_SEAT_NAME);
        assertThat(testSeat.isStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void getAllSeats() throws Exception {
        // Initialize the database
        seatRepository.saveAndFlush(seat);

        // Get all the seats
        restSeatMockMvc.perform(get("/api/seats?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(seat.getId().intValue())))
                .andExpect(jsonPath("$.[*].seatId").value(hasItem(DEFAULT_SEAT_ID)))
                .andExpect(jsonPath("$.[*].seatName").value(hasItem(DEFAULT_SEAT_NAME.toString())))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())));
    }

    @Test
    @Transactional
    public void getSeat() throws Exception {
        // Initialize the database
        seatRepository.saveAndFlush(seat);

        // Get the seat
        restSeatMockMvc.perform(get("/api/seats/{id}", seat.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(seat.getId().intValue()))
            .andExpect(jsonPath("$.seatId").value(DEFAULT_SEAT_ID))
            .andExpect(jsonPath("$.seatName").value(DEFAULT_SEAT_NAME.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSeat() throws Exception {
        // Get the seat
        restSeatMockMvc.perform(get("/api/seats/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSeat() throws Exception {
        // Initialize the database
        seatRepository.saveAndFlush(seat);
        int databaseSizeBeforeUpdate = seatRepository.findAll().size();

        // Update the seat
        Seat updatedSeat = new Seat();
        updatedSeat.setId(seat.getId());
        updatedSeat.setSeatId(UPDATED_SEAT_ID);
        updatedSeat.setSeatName(UPDATED_SEAT_NAME);
        updatedSeat.setStatus(UPDATED_STATUS);

        restSeatMockMvc.perform(put("/api/seats")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedSeat)))
                .andExpect(status().isOk());

        // Validate the Seat in the database
        List<Seat> seats = seatRepository.findAll();
        assertThat(seats).hasSize(databaseSizeBeforeUpdate);
        Seat testSeat = seats.get(seats.size() - 1);
        assertThat(testSeat.getSeatId()).isEqualTo(UPDATED_SEAT_ID);
        assertThat(testSeat.getSeatName()).isEqualTo(UPDATED_SEAT_NAME);
        assertThat(testSeat.isStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void deleteSeat() throws Exception {
        // Initialize the database
        seatRepository.saveAndFlush(seat);
        int databaseSizeBeforeDelete = seatRepository.findAll().size();

        // Get the seat
        restSeatMockMvc.perform(delete("/api/seats/{id}", seat.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Seat> seats = seatRepository.findAll();
        assertThat(seats).hasSize(databaseSizeBeforeDelete - 1);
    }
}
