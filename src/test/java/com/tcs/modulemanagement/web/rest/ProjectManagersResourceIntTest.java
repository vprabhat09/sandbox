package com.tcs.modulemanagement.web.rest;

import com.tcs.modulemanagement.ModulemanagementV3App;
import com.tcs.modulemanagement.domain.ProjectManagers;
import com.tcs.modulemanagement.repository.ProjectManagersRepository;
import com.tcs.modulemanagement.service.ProjectManagersService;
import com.tcs.modulemanagement.web.rest.dto.ProjectManagersDTO;
import com.tcs.modulemanagement.web.rest.mapper.ProjectManagersMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ProjectManagersResource REST controller.
 *
 * @see ProjectManagersResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ModulemanagementV3App.class)
@WebAppConfiguration
@IntegrationTest
public class ProjectManagersResourceIntTest {


    private static final Integer DEFAULT_PROJECT_MANAGER_ID = 1;
    private static final Integer UPDATED_PROJECT_MANAGER_ID = 2;

    @Inject
    private ProjectManagersRepository projectManagersRepository;

    @Inject
    private ProjectManagersMapper projectManagersMapper;

    @Inject
    private ProjectManagersService projectManagersService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restProjectManagersMockMvc;

    private ProjectManagers projectManagers;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ProjectManagersResource projectManagersResource = new ProjectManagersResource();
        ReflectionTestUtils.setField(projectManagersResource, "projectManagersService", projectManagersService);
        ReflectionTestUtils.setField(projectManagersResource, "projectManagersMapper", projectManagersMapper);
        this.restProjectManagersMockMvc = MockMvcBuilders.standaloneSetup(projectManagersResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        projectManagers = new ProjectManagers();
        projectManagers.setProjectManagerId(DEFAULT_PROJECT_MANAGER_ID);
    }

    @Test
    @Transactional
    public void createProjectManagers() throws Exception {
        int databaseSizeBeforeCreate = projectManagersRepository.findAll().size();

        // Create the ProjectManagers
        ProjectManagersDTO projectManagersDTO = projectManagersMapper.projectManagersToProjectManagersDTO(projectManagers);

        restProjectManagersMockMvc.perform(post("/api/project-managers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(projectManagersDTO)))
                .andExpect(status().isCreated());

        // Validate the ProjectManagers in the database
        List<ProjectManagers> projectManagers = projectManagersRepository.findAll();
        assertThat(projectManagers).hasSize(databaseSizeBeforeCreate + 1);
        ProjectManagers testProjectManagers = projectManagers.get(projectManagers.size() - 1);
        assertThat(testProjectManagers.getProjectManagerId()).isEqualTo(DEFAULT_PROJECT_MANAGER_ID);
    }

    @Test
    @Transactional
    public void getAllProjectManagers() throws Exception {
        // Initialize the database
        projectManagersRepository.saveAndFlush(projectManagers);

        // Get all the projectManagers
        restProjectManagersMockMvc.perform(get("/api/project-managers?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(projectManagers.getId().intValue())))
                .andExpect(jsonPath("$.[*].projectManagerId").value(hasItem(DEFAULT_PROJECT_MANAGER_ID)));
    }

    @Test
    @Transactional
    public void getProjectManagers() throws Exception {
        // Initialize the database
        projectManagersRepository.saveAndFlush(projectManagers);

        // Get the projectManagers
        restProjectManagersMockMvc.perform(get("/api/project-managers/{id}", projectManagers.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(projectManagers.getId().intValue()))
            .andExpect(jsonPath("$.projectManagerId").value(DEFAULT_PROJECT_MANAGER_ID));
    }

    @Test
    @Transactional
    public void getNonExistingProjectManagers() throws Exception {
        // Get the projectManagers
        restProjectManagersMockMvc.perform(get("/api/project-managers/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProjectManagers() throws Exception {
        // Initialize the database
        projectManagersRepository.saveAndFlush(projectManagers);
        int databaseSizeBeforeUpdate = projectManagersRepository.findAll().size();

        // Update the projectManagers
        ProjectManagers updatedProjectManagers = new ProjectManagers();
        updatedProjectManagers.setId(projectManagers.getId());
        updatedProjectManagers.setProjectManagerId(UPDATED_PROJECT_MANAGER_ID);
        ProjectManagersDTO projectManagersDTO = projectManagersMapper.projectManagersToProjectManagersDTO(updatedProjectManagers);

        restProjectManagersMockMvc.perform(put("/api/project-managers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(projectManagersDTO)))
                .andExpect(status().isOk());

        // Validate the ProjectManagers in the database
        List<ProjectManagers> projectManagers = projectManagersRepository.findAll();
        assertThat(projectManagers).hasSize(databaseSizeBeforeUpdate);
        ProjectManagers testProjectManagers = projectManagers.get(projectManagers.size() - 1);
        assertThat(testProjectManagers.getProjectManagerId()).isEqualTo(UPDATED_PROJECT_MANAGER_ID);
    }

    @Test
    @Transactional
    public void deleteProjectManagers() throws Exception {
        // Initialize the database
        projectManagersRepository.saveAndFlush(projectManagers);
        int databaseSizeBeforeDelete = projectManagersRepository.findAll().size();

        // Get the projectManagers
        restProjectManagersMockMvc.perform(delete("/api/project-managers/{id}", projectManagers.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ProjectManagers> projectManagers = projectManagersRepository.findAll();
        assertThat(projectManagers).hasSize(databaseSizeBeforeDelete - 1);
    }
}
