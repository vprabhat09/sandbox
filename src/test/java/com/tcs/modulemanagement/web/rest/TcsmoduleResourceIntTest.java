package com.tcs.modulemanagement.web.rest;

import com.tcs.modulemanagement.ModulemanagementV3App;
import com.tcs.modulemanagement.domain.Tcsmodule;
import com.tcs.modulemanagement.repository.TcsmoduleRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.tcs.modulemanagement.domain.enumeration.Wing;
import com.tcs.modulemanagement.domain.enumeration.BlockName;

/**
 * Test class for the TcsmoduleResource REST controller.
 *
 * @see TcsmoduleResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ModulemanagementV3App.class)
@WebAppConfiguration
@IntegrationTest
public class TcsmoduleResourceIntTest {


    private static final Integer DEFAULT_MODULE_ID = 1;
    private static final Integer UPDATED_MODULE_ID = 2;

    private static final Wing DEFAULT_WING = Wing.NW;
    private static final Wing UPDATED_WING = Wing.SW;

    private static final BlockName DEFAULT_BLOCK_NAME = BlockName.EB1;
    private static final BlockName UPDATED_BLOCK_NAME = BlockName.EB2;

    @Inject
    private TcsmoduleRepository tcsmoduleRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTcsmoduleMockMvc;

    private Tcsmodule tcsmodule;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TcsmoduleResource tcsmoduleResource = new TcsmoduleResource();
        ReflectionTestUtils.setField(tcsmoduleResource, "tcsmoduleRepository", tcsmoduleRepository);
        this.restTcsmoduleMockMvc = MockMvcBuilders.standaloneSetup(tcsmoduleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        tcsmodule = new Tcsmodule();
        tcsmodule.setModuleId(DEFAULT_MODULE_ID);
        tcsmodule.setWing(DEFAULT_WING);
        tcsmodule.setBlockName(DEFAULT_BLOCK_NAME);
    }

    @Test
    @Transactional
    public void createTcsmodule() throws Exception {
        int databaseSizeBeforeCreate = tcsmoduleRepository.findAll().size();

        // Create the Tcsmodule

        restTcsmoduleMockMvc.perform(post("/api/tcsmodules")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tcsmodule)))
                .andExpect(status().isCreated());

        // Validate the Tcsmodule in the database
        List<Tcsmodule> tcsmodules = tcsmoduleRepository.findAll();
        assertThat(tcsmodules).hasSize(databaseSizeBeforeCreate + 1);
        Tcsmodule testTcsmodule = tcsmodules.get(tcsmodules.size() - 1);
        assertThat(testTcsmodule.getModuleId()).isEqualTo(DEFAULT_MODULE_ID);
        assertThat(testTcsmodule.getWing()).isEqualTo(DEFAULT_WING);
        assertThat(testTcsmodule.getBlockName()).isEqualTo(DEFAULT_BLOCK_NAME);
    }

    @Test
    @Transactional
    public void getAllTcsmodules() throws Exception {
        // Initialize the database
        tcsmoduleRepository.saveAndFlush(tcsmodule);

        // Get all the tcsmodules
        restTcsmoduleMockMvc.perform(get("/api/tcsmodules?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(tcsmodule.getId().intValue())))
                .andExpect(jsonPath("$.[*].moduleId").value(hasItem(DEFAULT_MODULE_ID)))
                .andExpect(jsonPath("$.[*].wing").value(hasItem(DEFAULT_WING.toString())))
                .andExpect(jsonPath("$.[*].blockName").value(hasItem(DEFAULT_BLOCK_NAME.toString())));
    }

    @Test
    @Transactional
    public void getTcsmodule() throws Exception {
        // Initialize the database
        tcsmoduleRepository.saveAndFlush(tcsmodule);

        // Get the tcsmodule
        restTcsmoduleMockMvc.perform(get("/api/tcsmodules/{id}", tcsmodule.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(tcsmodule.getId().intValue()))
            .andExpect(jsonPath("$.moduleId").value(DEFAULT_MODULE_ID))
            .andExpect(jsonPath("$.wing").value(DEFAULT_WING.toString()))
            .andExpect(jsonPath("$.blockName").value(DEFAULT_BLOCK_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTcsmodule() throws Exception {
        // Get the tcsmodule
        restTcsmoduleMockMvc.perform(get("/api/tcsmodules/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTcsmodule() throws Exception {
        // Initialize the database
        tcsmoduleRepository.saveAndFlush(tcsmodule);
        int databaseSizeBeforeUpdate = tcsmoduleRepository.findAll().size();

        // Update the tcsmodule
        Tcsmodule updatedTcsmodule = new Tcsmodule();
        updatedTcsmodule.setId(tcsmodule.getId());
        updatedTcsmodule.setModuleId(UPDATED_MODULE_ID);
        updatedTcsmodule.setWing(UPDATED_WING);
        updatedTcsmodule.setBlockName(UPDATED_BLOCK_NAME);

        restTcsmoduleMockMvc.perform(put("/api/tcsmodules")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedTcsmodule)))
                .andExpect(status().isOk());

        // Validate the Tcsmodule in the database
        List<Tcsmodule> tcsmodules = tcsmoduleRepository.findAll();
        assertThat(tcsmodules).hasSize(databaseSizeBeforeUpdate);
        Tcsmodule testTcsmodule = tcsmodules.get(tcsmodules.size() - 1);
        assertThat(testTcsmodule.getModuleId()).isEqualTo(UPDATED_MODULE_ID);
        assertThat(testTcsmodule.getWing()).isEqualTo(UPDATED_WING);
        assertThat(testTcsmodule.getBlockName()).isEqualTo(UPDATED_BLOCK_NAME);
    }

    @Test
    @Transactional
    public void deleteTcsmodule() throws Exception {
        // Initialize the database
        tcsmoduleRepository.saveAndFlush(tcsmodule);
        int databaseSizeBeforeDelete = tcsmoduleRepository.findAll().size();

        // Get the tcsmodule
        restTcsmoduleMockMvc.perform(delete("/api/tcsmodules/{id}", tcsmodule.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Tcsmodule> tcsmodules = tcsmoduleRepository.findAll();
        assertThat(tcsmodules).hasSize(databaseSizeBeforeDelete - 1);
    }
}
