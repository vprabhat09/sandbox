package com.tcs.modulemanagement.web.rest;

import com.tcs.modulemanagement.ModulemanagementV3App;
import com.tcs.modulemanagement.domain.TeamLeads;
import com.tcs.modulemanagement.repository.TeamLeadsRepository;
import com.tcs.modulemanagement.service.TeamLeadsService;
import com.tcs.modulemanagement.web.rest.dto.TeamLeadsDTO;
import com.tcs.modulemanagement.web.rest.mapper.TeamLeadsMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TeamLeadsResource REST controller.
 *
 * @see TeamLeadsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ModulemanagementV3App.class)
@WebAppConfiguration
@IntegrationTest
public class TeamLeadsResourceIntTest {


    private static final Integer DEFAULT_TEAM_LEAD_ID = 1;
    private static final Integer UPDATED_TEAM_LEAD_ID = 2;

    @Inject
    private TeamLeadsRepository teamLeadsRepository;

    @Inject
    private TeamLeadsMapper teamLeadsMapper;

    @Inject
    private TeamLeadsService teamLeadsService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTeamLeadsMockMvc;

    private TeamLeads teamLeads;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TeamLeadsResource teamLeadsResource = new TeamLeadsResource();
        ReflectionTestUtils.setField(teamLeadsResource, "teamLeadsService", teamLeadsService);
        ReflectionTestUtils.setField(teamLeadsResource, "teamLeadsMapper", teamLeadsMapper);
        this.restTeamLeadsMockMvc = MockMvcBuilders.standaloneSetup(teamLeadsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        teamLeads = new TeamLeads();
        teamLeads.setTeamLeadId(DEFAULT_TEAM_LEAD_ID);
    }

    @Test
    @Transactional
    public void createTeamLeads() throws Exception {
        int databaseSizeBeforeCreate = teamLeadsRepository.findAll().size();

        // Create the TeamLeads
        TeamLeadsDTO teamLeadsDTO = teamLeadsMapper.teamLeadsToTeamLeadsDTO(teamLeads);

        restTeamLeadsMockMvc.perform(post("/api/team-leads")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(teamLeadsDTO)))
                .andExpect(status().isCreated());

        // Validate the TeamLeads in the database
        List<TeamLeads> teamLeads = teamLeadsRepository.findAll();
        assertThat(teamLeads).hasSize(databaseSizeBeforeCreate + 1);
        TeamLeads testTeamLeads = teamLeads.get(teamLeads.size() - 1);
        assertThat(testTeamLeads.getTeamLeadId()).isEqualTo(DEFAULT_TEAM_LEAD_ID);
    }

    @Test
    @Transactional
    public void getAllTeamLeads() throws Exception {
        // Initialize the database
        teamLeadsRepository.saveAndFlush(teamLeads);

        // Get all the teamLeads
        restTeamLeadsMockMvc.perform(get("/api/team-leads?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(teamLeads.getId().intValue())))
                .andExpect(jsonPath("$.[*].teamLeadId").value(hasItem(DEFAULT_TEAM_LEAD_ID)));
    }

    @Test
    @Transactional
    public void getTeamLeads() throws Exception {
        // Initialize the database
        teamLeadsRepository.saveAndFlush(teamLeads);

        // Get the teamLeads
        restTeamLeadsMockMvc.perform(get("/api/team-leads/{id}", teamLeads.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(teamLeads.getId().intValue()))
            .andExpect(jsonPath("$.teamLeadId").value(DEFAULT_TEAM_LEAD_ID));
    }

    @Test
    @Transactional
    public void getNonExistingTeamLeads() throws Exception {
        // Get the teamLeads
        restTeamLeadsMockMvc.perform(get("/api/team-leads/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTeamLeads() throws Exception {
        // Initialize the database
        teamLeadsRepository.saveAndFlush(teamLeads);
        int databaseSizeBeforeUpdate = teamLeadsRepository.findAll().size();

        // Update the teamLeads
        TeamLeads updatedTeamLeads = new TeamLeads();
        updatedTeamLeads.setId(teamLeads.getId());
        updatedTeamLeads.setTeamLeadId(UPDATED_TEAM_LEAD_ID);
        TeamLeadsDTO teamLeadsDTO = teamLeadsMapper.teamLeadsToTeamLeadsDTO(updatedTeamLeads);

        restTeamLeadsMockMvc.perform(put("/api/team-leads")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(teamLeadsDTO)))
                .andExpect(status().isOk());

        // Validate the TeamLeads in the database
        List<TeamLeads> teamLeads = teamLeadsRepository.findAll();
        assertThat(teamLeads).hasSize(databaseSizeBeforeUpdate);
        TeamLeads testTeamLeads = teamLeads.get(teamLeads.size() - 1);
        assertThat(testTeamLeads.getTeamLeadId()).isEqualTo(UPDATED_TEAM_LEAD_ID);
    }

    @Test
    @Transactional
    public void deleteTeamLeads() throws Exception {
        // Initialize the database
        teamLeadsRepository.saveAndFlush(teamLeads);
        int databaseSizeBeforeDelete = teamLeadsRepository.findAll().size();

        // Get the teamLeads
        restTeamLeadsMockMvc.perform(delete("/api/team-leads/{id}", teamLeads.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<TeamLeads> teamLeads = teamLeadsRepository.findAll();
        assertThat(teamLeads).hasSize(databaseSizeBeforeDelete - 1);
    }
}
