package com.tcs.modulemanagement.web.rest;

import com.tcs.modulemanagement.ModulemanagementV3App;
import com.tcs.modulemanagement.domain.TeamManagers;
import com.tcs.modulemanagement.repository.TeamManagersRepository;
import com.tcs.modulemanagement.service.TeamManagersService;
import com.tcs.modulemanagement.web.rest.dto.TeamManagersDTO;
import com.tcs.modulemanagement.web.rest.mapper.TeamManagersMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TeamManagersResource REST controller.
 *
 * @see TeamManagersResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ModulemanagementV3App.class)
@WebAppConfiguration
@IntegrationTest
public class TeamManagersResourceIntTest {


    private static final Integer DEFAULT_TEAM_MANAGER_ID = 1;
    private static final Integer UPDATED_TEAM_MANAGER_ID = 2;

    @Inject
    private TeamManagersRepository teamManagersRepository;

    @Inject
    private TeamManagersMapper teamManagersMapper;

    @Inject
    private TeamManagersService teamManagersService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTeamManagersMockMvc;

    private TeamManagers teamManagers;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TeamManagersResource teamManagersResource = new TeamManagersResource();
        ReflectionTestUtils.setField(teamManagersResource, "teamManagersService", teamManagersService);
        ReflectionTestUtils.setField(teamManagersResource, "teamManagersMapper", teamManagersMapper);
        this.restTeamManagersMockMvc = MockMvcBuilders.standaloneSetup(teamManagersResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        teamManagers = new TeamManagers();
        teamManagers.setTeamManagerId(DEFAULT_TEAM_MANAGER_ID);
    }

    @Test
    @Transactional
    public void createTeamManagers() throws Exception {
        int databaseSizeBeforeCreate = teamManagersRepository.findAll().size();

        // Create the TeamManagers
        TeamManagersDTO teamManagersDTO = teamManagersMapper.teamManagersToTeamManagersDTO(teamManagers);

        restTeamManagersMockMvc.perform(post("/api/team-managers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(teamManagersDTO)))
                .andExpect(status().isCreated());

        // Validate the TeamManagers in the database
        List<TeamManagers> teamManagers = teamManagersRepository.findAll();
        assertThat(teamManagers).hasSize(databaseSizeBeforeCreate + 1);
        TeamManagers testTeamManagers = teamManagers.get(teamManagers.size() - 1);
        assertThat(testTeamManagers.getTeamManagerId()).isEqualTo(DEFAULT_TEAM_MANAGER_ID);
    }

    @Test
    @Transactional
    public void getAllTeamManagers() throws Exception {
        // Initialize the database
        teamManagersRepository.saveAndFlush(teamManagers);

        // Get all the teamManagers
        restTeamManagersMockMvc.perform(get("/api/team-managers?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(teamManagers.getId().intValue())))
                .andExpect(jsonPath("$.[*].teamManagerId").value(hasItem(DEFAULT_TEAM_MANAGER_ID)));
    }

    @Test
    @Transactional
    public void getTeamManagers() throws Exception {
        // Initialize the database
        teamManagersRepository.saveAndFlush(teamManagers);

        // Get the teamManagers
        restTeamManagersMockMvc.perform(get("/api/team-managers/{id}", teamManagers.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(teamManagers.getId().intValue()))
            .andExpect(jsonPath("$.teamManagerId").value(DEFAULT_TEAM_MANAGER_ID));
    }

    @Test
    @Transactional
    public void getNonExistingTeamManagers() throws Exception {
        // Get the teamManagers
        restTeamManagersMockMvc.perform(get("/api/team-managers/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTeamManagers() throws Exception {
        // Initialize the database
        teamManagersRepository.saveAndFlush(teamManagers);
        int databaseSizeBeforeUpdate = teamManagersRepository.findAll().size();

        // Update the teamManagers
        TeamManagers updatedTeamManagers = new TeamManagers();
        updatedTeamManagers.setId(teamManagers.getId());
        updatedTeamManagers.setTeamManagerId(UPDATED_TEAM_MANAGER_ID);
        TeamManagersDTO teamManagersDTO = teamManagersMapper.teamManagersToTeamManagersDTO(updatedTeamManagers);

        restTeamManagersMockMvc.perform(put("/api/team-managers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(teamManagersDTO)))
                .andExpect(status().isOk());

        // Validate the TeamManagers in the database
        List<TeamManagers> teamManagers = teamManagersRepository.findAll();
        assertThat(teamManagers).hasSize(databaseSizeBeforeUpdate);
        TeamManagers testTeamManagers = teamManagers.get(teamManagers.size() - 1);
        assertThat(testTeamManagers.getTeamManagerId()).isEqualTo(UPDATED_TEAM_MANAGER_ID);
    }

    @Test
    @Transactional
    public void deleteTeamManagers() throws Exception {
        // Initialize the database
        teamManagersRepository.saveAndFlush(teamManagers);
        int databaseSizeBeforeDelete = teamManagersRepository.findAll().size();

        // Get the teamManagers
        restTeamManagersMockMvc.perform(delete("/api/team-managers/{id}", teamManagers.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<TeamManagers> teamManagers = teamManagersRepository.findAll();
        assertThat(teamManagers).hasSize(databaseSizeBeforeDelete - 1);
    }
}
