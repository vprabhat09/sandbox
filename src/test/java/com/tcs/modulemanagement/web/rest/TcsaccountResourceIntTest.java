package com.tcs.modulemanagement.web.rest;

import com.tcs.modulemanagement.ModulemanagementV3App;
import com.tcs.modulemanagement.domain.Tcsaccount;
import com.tcs.modulemanagement.repository.TcsaccountRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TcsaccountResource REST controller.
 *
 * @see TcsaccountResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ModulemanagementV3App.class)
@WebAppConfiguration
@IntegrationTest
public class TcsaccountResourceIntTest {


    private static final Integer DEFAULT_ACCOUNT_ID = 1;
    private static final Integer UPDATED_ACCOUNT_ID = 2;
    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_CLIENT = "AAAAA";
    private static final String UPDATED_CLIENT = "BBBBB";

    @Inject
    private TcsaccountRepository tcsaccountRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTcsaccountMockMvc;

    private Tcsaccount tcsaccount;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TcsaccountResource tcsaccountResource = new TcsaccountResource();
        ReflectionTestUtils.setField(tcsaccountResource, "tcsaccountRepository", tcsaccountRepository);
        this.restTcsaccountMockMvc = MockMvcBuilders.standaloneSetup(tcsaccountResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        tcsaccount = new Tcsaccount();
        tcsaccount.setAccountId(DEFAULT_ACCOUNT_ID);
        tcsaccount.setName(DEFAULT_NAME);
        tcsaccount.setClient(DEFAULT_CLIENT);
    }

    @Test
    @Transactional
    public void createTcsaccount() throws Exception {
        int databaseSizeBeforeCreate = tcsaccountRepository.findAll().size();

        // Create the Tcsaccount

        restTcsaccountMockMvc.perform(post("/api/tcsaccounts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tcsaccount)))
                .andExpect(status().isCreated());

        // Validate the Tcsaccount in the database
        List<Tcsaccount> tcsaccounts = tcsaccountRepository.findAll();
        assertThat(tcsaccounts).hasSize(databaseSizeBeforeCreate + 1);
        Tcsaccount testTcsaccount = tcsaccounts.get(tcsaccounts.size() - 1);
        assertThat(testTcsaccount.getAccountId()).isEqualTo(DEFAULT_ACCOUNT_ID);
        assertThat(testTcsaccount.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTcsaccount.getClient()).isEqualTo(DEFAULT_CLIENT);
    }

    @Test
    @Transactional
    public void getAllTcsaccounts() throws Exception {
        // Initialize the database
        tcsaccountRepository.saveAndFlush(tcsaccount);

        // Get all the tcsaccounts
        restTcsaccountMockMvc.perform(get("/api/tcsaccounts?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(tcsaccount.getId().intValue())))
                .andExpect(jsonPath("$.[*].accountId").value(hasItem(DEFAULT_ACCOUNT_ID)))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].client").value(hasItem(DEFAULT_CLIENT.toString())));
    }

    @Test
    @Transactional
    public void getTcsaccount() throws Exception {
        // Initialize the database
        tcsaccountRepository.saveAndFlush(tcsaccount);

        // Get the tcsaccount
        restTcsaccountMockMvc.perform(get("/api/tcsaccounts/{id}", tcsaccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(tcsaccount.getId().intValue()))
            .andExpect(jsonPath("$.accountId").value(DEFAULT_ACCOUNT_ID))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.client").value(DEFAULT_CLIENT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTcsaccount() throws Exception {
        // Get the tcsaccount
        restTcsaccountMockMvc.perform(get("/api/tcsaccounts/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTcsaccount() throws Exception {
        // Initialize the database
        tcsaccountRepository.saveAndFlush(tcsaccount);
        int databaseSizeBeforeUpdate = tcsaccountRepository.findAll().size();

        // Update the tcsaccount
        Tcsaccount updatedTcsaccount = new Tcsaccount();
        updatedTcsaccount.setId(tcsaccount.getId());
        updatedTcsaccount.setAccountId(UPDATED_ACCOUNT_ID);
        updatedTcsaccount.setName(UPDATED_NAME);
        updatedTcsaccount.setClient(UPDATED_CLIENT);

        restTcsaccountMockMvc.perform(put("/api/tcsaccounts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedTcsaccount)))
                .andExpect(status().isOk());

        // Validate the Tcsaccount in the database
        List<Tcsaccount> tcsaccounts = tcsaccountRepository.findAll();
        assertThat(tcsaccounts).hasSize(databaseSizeBeforeUpdate);
        Tcsaccount testTcsaccount = tcsaccounts.get(tcsaccounts.size() - 1);
        assertThat(testTcsaccount.getAccountId()).isEqualTo(UPDATED_ACCOUNT_ID);
        assertThat(testTcsaccount.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTcsaccount.getClient()).isEqualTo(UPDATED_CLIENT);
    }

    @Test
    @Transactional
    public void deleteTcsaccount() throws Exception {
        // Initialize the database
        tcsaccountRepository.saveAndFlush(tcsaccount);
        int databaseSizeBeforeDelete = tcsaccountRepository.findAll().size();

        // Get the tcsaccount
        restTcsaccountMockMvc.perform(delete("/api/tcsaccounts/{id}", tcsaccount.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Tcsaccount> tcsaccounts = tcsaccountRepository.findAll();
        assertThat(tcsaccounts).hasSize(databaseSizeBeforeDelete - 1);
    }
}
