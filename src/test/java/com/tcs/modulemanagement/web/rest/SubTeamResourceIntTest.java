package com.tcs.modulemanagement.web.rest;

import com.tcs.modulemanagement.ModulemanagementV3App;
import com.tcs.modulemanagement.domain.SubTeam;
import com.tcs.modulemanagement.repository.SubTeamRepository;
import com.tcs.modulemanagement.service.SubTeamService;
import com.tcs.modulemanagement.web.rest.dto.SubTeamDTO;
import com.tcs.modulemanagement.web.rest.mapper.SubTeamMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SubTeamResource REST controller.
 *
 * @see SubTeamResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ModulemanagementV3App.class)
@WebAppConfiguration
@IntegrationTest
public class SubTeamResourceIntTest {


    private static final Integer DEFAULT_SUB_TEAM_ID = 1;
    private static final Integer UPDATED_SUB_TEAM_ID = 2;
    private static final String DEFAULT_SUB_TEAM_NAME = "AAAAA";
    private static final String UPDATED_SUB_TEAM_NAME = "BBBBB";

    @Inject
    private SubTeamRepository subTeamRepository;

    @Inject
    private SubTeamMapper subTeamMapper;

    @Inject
    private SubTeamService subTeamService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSubTeamMockMvc;

    private SubTeam subTeam;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SubTeamResource subTeamResource = new SubTeamResource();
        ReflectionTestUtils.setField(subTeamResource, "subTeamService", subTeamService);
        ReflectionTestUtils.setField(subTeamResource, "subTeamMapper", subTeamMapper);
        this.restSubTeamMockMvc = MockMvcBuilders.standaloneSetup(subTeamResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        subTeam = new SubTeam();
        subTeam.setSubTeamId(DEFAULT_SUB_TEAM_ID);
        subTeam.setSubTeamName(DEFAULT_SUB_TEAM_NAME);
    }

    @Test
    @Transactional
    public void createSubTeam() throws Exception {
        int databaseSizeBeforeCreate = subTeamRepository.findAll().size();

        // Create the SubTeam
        SubTeamDTO subTeamDTO = subTeamMapper.subTeamToSubTeamDTO(subTeam);

        restSubTeamMockMvc.perform(post("/api/sub-teams")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(subTeamDTO)))
                .andExpect(status().isCreated());

        // Validate the SubTeam in the database
        List<SubTeam> subTeams = subTeamRepository.findAll();
        assertThat(subTeams).hasSize(databaseSizeBeforeCreate + 1);
        SubTeam testSubTeam = subTeams.get(subTeams.size() - 1);
        assertThat(testSubTeam.getSubTeamId()).isEqualTo(DEFAULT_SUB_TEAM_ID);
        assertThat(testSubTeam.getSubTeamName()).isEqualTo(DEFAULT_SUB_TEAM_NAME);
    }

    @Test
    @Transactional
    public void getAllSubTeams() throws Exception {
        // Initialize the database
        subTeamRepository.saveAndFlush(subTeam);

        // Get all the subTeams
        restSubTeamMockMvc.perform(get("/api/sub-teams?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(subTeam.getId().intValue())))
                .andExpect(jsonPath("$.[*].subTeamId").value(hasItem(DEFAULT_SUB_TEAM_ID)))
                .andExpect(jsonPath("$.[*].subTeamName").value(hasItem(DEFAULT_SUB_TEAM_NAME.toString())));
    }

    @Test
    @Transactional
    public void getSubTeam() throws Exception {
        // Initialize the database
        subTeamRepository.saveAndFlush(subTeam);

        // Get the subTeam
        restSubTeamMockMvc.perform(get("/api/sub-teams/{id}", subTeam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(subTeam.getId().intValue()))
            .andExpect(jsonPath("$.subTeamId").value(DEFAULT_SUB_TEAM_ID))
            .andExpect(jsonPath("$.subTeamName").value(DEFAULT_SUB_TEAM_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSubTeam() throws Exception {
        // Get the subTeam
        restSubTeamMockMvc.perform(get("/api/sub-teams/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSubTeam() throws Exception {
        // Initialize the database
        subTeamRepository.saveAndFlush(subTeam);
        int databaseSizeBeforeUpdate = subTeamRepository.findAll().size();

        // Update the subTeam
        SubTeam updatedSubTeam = new SubTeam();
        updatedSubTeam.setId(subTeam.getId());
        updatedSubTeam.setSubTeamId(UPDATED_SUB_TEAM_ID);
        updatedSubTeam.setSubTeamName(UPDATED_SUB_TEAM_NAME);
        SubTeamDTO subTeamDTO = subTeamMapper.subTeamToSubTeamDTO(updatedSubTeam);

        restSubTeamMockMvc.perform(put("/api/sub-teams")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(subTeamDTO)))
                .andExpect(status().isOk());

        // Validate the SubTeam in the database
        List<SubTeam> subTeams = subTeamRepository.findAll();
        assertThat(subTeams).hasSize(databaseSizeBeforeUpdate);
        SubTeam testSubTeam = subTeams.get(subTeams.size() - 1);
        assertThat(testSubTeam.getSubTeamId()).isEqualTo(UPDATED_SUB_TEAM_ID);
        assertThat(testSubTeam.getSubTeamName()).isEqualTo(UPDATED_SUB_TEAM_NAME);
    }

    @Test
    @Transactional
    public void deleteSubTeam() throws Exception {
        // Initialize the database
        subTeamRepository.saveAndFlush(subTeam);
        int databaseSizeBeforeDelete = subTeamRepository.findAll().size();

        // Get the subTeam
        restSubTeamMockMvc.perform(delete("/api/sub-teams/{id}", subTeam.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<SubTeam> subTeams = subTeamRepository.findAll();
        assertThat(subTeams).hasSize(databaseSizeBeforeDelete - 1);
    }
}
