package com.tcs.modulemanagement.web.rest;

import com.tcs.modulemanagement.ModulemanagementV3App;
import com.tcs.modulemanagement.domain.AccountManagers;
import com.tcs.modulemanagement.repository.AccountManagersRepository;
import com.tcs.modulemanagement.service.AccountManagersService;
import com.tcs.modulemanagement.web.rest.dto.AccountManagersDTO;
import com.tcs.modulemanagement.web.rest.mapper.AccountManagersMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the AccountManagersResource REST controller.
 *
 * @see AccountManagersResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ModulemanagementV3App.class)
@WebAppConfiguration
@IntegrationTest
public class AccountManagersResourceIntTest {


    private static final Integer DEFAULT_MANAGER_ID = 1;
    private static final Integer UPDATED_MANAGER_ID = 2;

    @Inject
    private AccountManagersRepository accountManagersRepository;

    @Inject
    private AccountManagersMapper accountManagersMapper;

    @Inject
    private AccountManagersService accountManagersService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restAccountManagersMockMvc;

    private AccountManagers accountManagers;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AccountManagersResource accountManagersResource = new AccountManagersResource();
        ReflectionTestUtils.setField(accountManagersResource, "accountManagersService", accountManagersService);
        ReflectionTestUtils.setField(accountManagersResource, "accountManagersMapper", accountManagersMapper);
        this.restAccountManagersMockMvc = MockMvcBuilders.standaloneSetup(accountManagersResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        accountManagers = new AccountManagers();
        accountManagers.setManagerId(DEFAULT_MANAGER_ID);
    }

    @Test
    @Transactional
    public void createAccountManagers() throws Exception {
        int databaseSizeBeforeCreate = accountManagersRepository.findAll().size();

        // Create the AccountManagers
        AccountManagersDTO accountManagersDTO = accountManagersMapper.accountManagersToAccountManagersDTO(accountManagers);

        restAccountManagersMockMvc.perform(post("/api/account-managers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(accountManagersDTO)))
                .andExpect(status().isCreated());

        // Validate the AccountManagers in the database
        List<AccountManagers> accountManagers = accountManagersRepository.findAll();
        assertThat(accountManagers).hasSize(databaseSizeBeforeCreate + 1);
        AccountManagers testAccountManagers = accountManagers.get(accountManagers.size() - 1);
        assertThat(testAccountManagers.getManagerId()).isEqualTo(DEFAULT_MANAGER_ID);
    }

    @Test
    @Transactional
    public void getAllAccountManagers() throws Exception {
        // Initialize the database
        accountManagersRepository.saveAndFlush(accountManagers);

        // Get all the accountManagers
        restAccountManagersMockMvc.perform(get("/api/account-managers?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(accountManagers.getId().intValue())))
                .andExpect(jsonPath("$.[*].managerId").value(hasItem(DEFAULT_MANAGER_ID)));
    }

    @Test
    @Transactional
    public void getAccountManagers() throws Exception {
        // Initialize the database
        accountManagersRepository.saveAndFlush(accountManagers);

        // Get the accountManagers
        restAccountManagersMockMvc.perform(get("/api/account-managers/{id}", accountManagers.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(accountManagers.getId().intValue()))
            .andExpect(jsonPath("$.managerId").value(DEFAULT_MANAGER_ID));
    }

    @Test
    @Transactional
    public void getNonExistingAccountManagers() throws Exception {
        // Get the accountManagers
        restAccountManagersMockMvc.perform(get("/api/account-managers/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAccountManagers() throws Exception {
        // Initialize the database
        accountManagersRepository.saveAndFlush(accountManagers);
        int databaseSizeBeforeUpdate = accountManagersRepository.findAll().size();

        // Update the accountManagers
        AccountManagers updatedAccountManagers = new AccountManagers();
        updatedAccountManagers.setId(accountManagers.getId());
        updatedAccountManagers.setManagerId(UPDATED_MANAGER_ID);
        AccountManagersDTO accountManagersDTO = accountManagersMapper.accountManagersToAccountManagersDTO(updatedAccountManagers);

        restAccountManagersMockMvc.perform(put("/api/account-managers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(accountManagersDTO)))
                .andExpect(status().isOk());

        // Validate the AccountManagers in the database
        List<AccountManagers> accountManagers = accountManagersRepository.findAll();
        assertThat(accountManagers).hasSize(databaseSizeBeforeUpdate);
        AccountManagers testAccountManagers = accountManagers.get(accountManagers.size() - 1);
        assertThat(testAccountManagers.getManagerId()).isEqualTo(UPDATED_MANAGER_ID);
    }

    @Test
    @Transactional
    public void deleteAccountManagers() throws Exception {
        // Initialize the database
        accountManagersRepository.saveAndFlush(accountManagers);
        int databaseSizeBeforeDelete = accountManagersRepository.findAll().size();

        // Get the accountManagers
        restAccountManagersMockMvc.perform(delete("/api/account-managers/{id}", accountManagers.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<AccountManagers> accountManagers = accountManagersRepository.findAll();
        assertThat(accountManagers).hasSize(databaseSizeBeforeDelete - 1);
    }
}
