package com.tcs.modulemanagement.web.rest;

import com.tcs.modulemanagement.ModulemanagementV3App;
import com.tcs.modulemanagement.domain.TeamMembers;
import com.tcs.modulemanagement.repository.TeamMembersRepository;
import com.tcs.modulemanagement.service.TeamMembersService;
import com.tcs.modulemanagement.web.rest.dto.TeamMembersDTO;
import com.tcs.modulemanagement.web.rest.mapper.TeamMembersMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TeamMembersResource REST controller.
 *
 * @see TeamMembersResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ModulemanagementV3App.class)
@WebAppConfiguration
@IntegrationTest
public class TeamMembersResourceIntTest {


    private static final Integer DEFAULT_MEMBER_ID = 1;
    private static final Integer UPDATED_MEMBER_ID = 2;

    @Inject
    private TeamMembersRepository teamMembersRepository;

    @Inject
    private TeamMembersMapper teamMembersMapper;

    @Inject
    private TeamMembersService teamMembersService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTeamMembersMockMvc;

    private TeamMembers teamMembers;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TeamMembersResource teamMembersResource = new TeamMembersResource();
        ReflectionTestUtils.setField(teamMembersResource, "teamMembersService", teamMembersService);
        ReflectionTestUtils.setField(teamMembersResource, "teamMembersMapper", teamMembersMapper);
        this.restTeamMembersMockMvc = MockMvcBuilders.standaloneSetup(teamMembersResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        teamMembers = new TeamMembers();
        teamMembers.setMemberId(DEFAULT_MEMBER_ID);
    }

    @Test
    @Transactional
    public void createTeamMembers() throws Exception {
        int databaseSizeBeforeCreate = teamMembersRepository.findAll().size();

        // Create the TeamMembers
        TeamMembersDTO teamMembersDTO = teamMembersMapper.teamMembersToTeamMembersDTO(teamMembers);

        restTeamMembersMockMvc.perform(post("/api/team-members")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(teamMembersDTO)))
                .andExpect(status().isCreated());

        // Validate the TeamMembers in the database
        List<TeamMembers> teamMembers = teamMembersRepository.findAll();
        assertThat(teamMembers).hasSize(databaseSizeBeforeCreate + 1);
        TeamMembers testTeamMembers = teamMembers.get(teamMembers.size() - 1);
        assertThat(testTeamMembers.getMemberId()).isEqualTo(DEFAULT_MEMBER_ID);
    }

    @Test
    @Transactional
    public void getAllTeamMembers() throws Exception {
        // Initialize the database
        teamMembersRepository.saveAndFlush(teamMembers);

        // Get all the teamMembers
        restTeamMembersMockMvc.perform(get("/api/team-members?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(teamMembers.getId().intValue())))
                .andExpect(jsonPath("$.[*].memberId").value(hasItem(DEFAULT_MEMBER_ID)));
    }

    @Test
    @Transactional
    public void getTeamMembers() throws Exception {
        // Initialize the database
        teamMembersRepository.saveAndFlush(teamMembers);

        // Get the teamMembers
        restTeamMembersMockMvc.perform(get("/api/team-members/{id}", teamMembers.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(teamMembers.getId().intValue()))
            .andExpect(jsonPath("$.memberId").value(DEFAULT_MEMBER_ID));
    }

    @Test
    @Transactional
    public void getNonExistingTeamMembers() throws Exception {
        // Get the teamMembers
        restTeamMembersMockMvc.perform(get("/api/team-members/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTeamMembers() throws Exception {
        // Initialize the database
        teamMembersRepository.saveAndFlush(teamMembers);
        int databaseSizeBeforeUpdate = teamMembersRepository.findAll().size();

        // Update the teamMembers
        TeamMembers updatedTeamMembers = new TeamMembers();
        updatedTeamMembers.setId(teamMembers.getId());
        updatedTeamMembers.setMemberId(UPDATED_MEMBER_ID);
        TeamMembersDTO teamMembersDTO = teamMembersMapper.teamMembersToTeamMembersDTO(updatedTeamMembers);

        restTeamMembersMockMvc.perform(put("/api/team-members")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(teamMembersDTO)))
                .andExpect(status().isOk());

        // Validate the TeamMembers in the database
        List<TeamMembers> teamMembers = teamMembersRepository.findAll();
        assertThat(teamMembers).hasSize(databaseSizeBeforeUpdate);
        TeamMembers testTeamMembers = teamMembers.get(teamMembers.size() - 1);
        assertThat(testTeamMembers.getMemberId()).isEqualTo(UPDATED_MEMBER_ID);
    }

    @Test
    @Transactional
    public void deleteTeamMembers() throws Exception {
        // Initialize the database
        teamMembersRepository.saveAndFlush(teamMembers);
        int databaseSizeBeforeDelete = teamMembersRepository.findAll().size();

        // Get the teamMembers
        restTeamMembersMockMvc.perform(delete("/api/team-members/{id}", teamMembers.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<TeamMembers> teamMembers = teamMembersRepository.findAll();
        assertThat(teamMembers).hasSize(databaseSizeBeforeDelete - 1);
    }
}
