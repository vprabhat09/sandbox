'use strict';

describe('Controller Tests', function() {

    describe('Employee Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockEmployee, MockAccountManagers, MockTeamManagers, MockProjectManagers, MockTeamLeads, MockTeamMembers, MockSeat;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockEmployee = jasmine.createSpy('MockEmployee');
            MockAccountManagers = jasmine.createSpy('MockAccountManagers');
            MockTeamManagers = jasmine.createSpy('MockTeamManagers');
            MockProjectManagers = jasmine.createSpy('MockProjectManagers');
            MockTeamLeads = jasmine.createSpy('MockTeamLeads');
            MockTeamMembers = jasmine.createSpy('MockTeamMembers');
            MockSeat = jasmine.createSpy('MockSeat');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Employee': MockEmployee,
                'AccountManagers': MockAccountManagers,
                'TeamManagers': MockTeamManagers,
                'ProjectManagers': MockProjectManagers,
                'TeamLeads': MockTeamLeads,
                'TeamMembers': MockTeamMembers,
                'Seat': MockSeat
            };
            createController = function() {
                $injector.get('$controller')("EmployeeDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'modulemanagementV3App:employeeUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
