'use strict';

describe('Controller Tests', function() {

    describe('Seat Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockSeat, MockEmployee, MockTcsmodule;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockSeat = jasmine.createSpy('MockSeat');
            MockEmployee = jasmine.createSpy('MockEmployee');
            MockTcsmodule = jasmine.createSpy('MockTcsmodule');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Seat': MockSeat,
                'Employee': MockEmployee,
                'Tcsmodule': MockTcsmodule
            };
            createController = function() {
                $injector.get('$controller')("SeatDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'modulemanagementV3App:seatUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
