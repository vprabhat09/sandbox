'use strict';

describe('Controller Tests', function() {

    describe('TeamLeads Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockTeamLeads, MockEmployee, MockSubTeam;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockTeamLeads = jasmine.createSpy('MockTeamLeads');
            MockEmployee = jasmine.createSpy('MockEmployee');
            MockSubTeam = jasmine.createSpy('MockSubTeam');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'TeamLeads': MockTeamLeads,
                'Employee': MockEmployee,
                'SubTeam': MockSubTeam
            };
            createController = function() {
                $injector.get('$controller')("TeamLeadsDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'modulemanagementV3App:teamLeadsUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
